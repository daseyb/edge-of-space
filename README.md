# EDGE OF SPACE

![Logo](presentation/screenshots/Screenshot%202016-02-08%2022.45.23.png)
![Screenshot](presentation/screenshots/Screenshot%202016-02-08%2022.47.51.png)

## Build Instruction
All dependencies are included. You should be able to run:

- ./build.sh
- ./run.sh

without issues.

## Gameplay Instructions

### Intro
- Move the little spaceship with WASD
- Shoot with SPACEBAR
- Move off screen to the right to quit the intro
- Press ENTER to skip the intro

### Game
- Look around with the mouse
- Yaw with A/D
- Pitch with W/S
- Roll with Q/E
- Accelerate/Decelerate with X/Y
- Press SHIFT for faster acceleration
- Press SPACEBAR to stop immediately

### Please play with sound (at least the intro)! :)

## Developers
- Dario Seyb
- Kaspar Scharf
- David Gilbert

## Music Credits: Jean Meurice (jean.meurice@rwth-aachen.de)