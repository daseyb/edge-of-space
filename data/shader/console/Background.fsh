#pragma import "../setup/vertex.glsl"

#pragma import "../CommonDeferredFrag.glsl"
#pragma import "../Utils.glsl"


void initShader(){};

vec4 color() {
  return vec4(0, 0, 0, 1);
}

vec4 emissive() {
  vec2 texCoord = vTexCoord;

  if(uTime > 7.5) {
    float time = (uTime - 7.5);
    texCoord.x += time * 0.2 * min(time, 2) * 0.5;
  }
  
  float middle = pow(abs(texCoord.y-0.5), 0.5);
  float val = texture(uTexture, vec2(-0.15, 0) + vTexCoord).r * pow((1.0- middle), 7);
    
  float octave01 = sin(texCoord.x * 100 + uTime) * 0.02 + val * 40;
  float octave02 = sin(texCoord.x * 200  + 2 + uTime * 3) * 0.01;

    
  float period = mod( abs(texCoord.x-0.5)  + uTime * 0.2 + middle + octave01 + octave02, 0.4 );
  vec4 res = vec4(2,  3 + sin(uTime * 0.6), 3 + cos(uTime*0.6), 1) * (0.1 + 0.6 * period) * 0.2;

  res += res * val * 200;
  res.a = 1;
  return res * uEmissiveColor;
}

vec3 normal() {
  return vNormal;
}

vec4 specularSmoothness() {
    return vec4(0, 0, 0, 0);
}
