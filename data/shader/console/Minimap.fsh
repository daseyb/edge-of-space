#pragma import "../setup/vertex.glsl"

#pragma import "../CommonDeferredFrag.glsl"
#pragma import "../Utils.glsl"


void initShader(){};

vec4 color() {
  return uTintColor;
}

vec4 emissive() {
  return uEmissiveColor;
}

vec3 normal() {
  return vNormal;
}

vec4 specularSmoothness() {
    return vec4(0, 0, 0, 0);
}
