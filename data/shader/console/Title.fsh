#pragma import "../setup/vertex.glsl"

#pragma import "../CommonDeferredFrag.glsl"
#pragma import "../Utils.glsl"

void initShader(){};

vec4 color() {
  return vec4(0, 0, 0, 1);
}

vec4 emissive() {
  vec4 res = vec4(0.6 + 0.5 * vTexCoord.x,  0.2  + 0.4 * vTexCoord.y, 0.2, 1) * (sin(uTime * 20) * cos(uTime * 2) * 0.5 + 1.4) * min(uTime/10 + 0.1, 1);
  res.a = 1;
  return res * uEmissiveColor;
}

vec3 normal() {
  return vNormal;
}

vec4 specularSmoothness() {
    return vec4(0, 0, 0, 0);
}
