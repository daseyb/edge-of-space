#pragma import "CommonTransparentFrag.glsl"
#pragma import "noise/noise3d.glsl"
#pragma import "Utils.glsl"
uniform vec3 uObjectPosition;
uniform vec3 uSunDir;

uniform sampler2D uSamplerColor;

float Rg;
float Rt;

const float ISun = 4.0;
const vec3 betaR = vec3(5.8e-3, 1.35e-2, 3.31e-2);

const float mieG = 0.8;

const int RES_R = 32;
const int RES_MU = 128;
const int RES_MU_S = 32;
const int RES_NU = 8;

void initShader(){};

vec3 unpackWorldPosition(float depth) {
    vec4 clipSpaceLocation;
    clipSpaceLocation.xy = gl_FragCoord.xy * uOneOverScreenSize * 2.0 - 1.0;
    clipSpaceLocation.z = depth * 2.0 - 1.0;
    clipSpaceLocation.w = 1.0;
    vec4 homogenousLocation = uViewProjectionInverseMatrix * clipSpaceLocation;
    return homogenousLocation.xyz/homogenousLocation.w;
}

vec3 intersectSphere(vec3 o, vec3 l, vec3 c, float r) {
	float a = -dot(l, o-c);
	float d = a * a - dot(o-c, o-c) + r * r;
	if(d < 0) {
		return vec3(0);
	}
	
	float dsqrt = sqrt(d);
	
	return o + l * max(0, min(a + dsqrt, a - dsqrt));
}

vec3 intersectSphereMax(vec3 o, vec3 l, vec3 c, float r) {
	float a = -dot(l, o-c);
	float d = a * a - dot(o-c, o-c) + r * r;
	if(d < 0) {
		return vec3(0);
	}
	
	float dsqrt = sqrt(d);
	
	return o + l * max(0, max(a + dsqrt, a - dsqrt));
}


float pow8(float val) {
  val = val * val;
  val *= val;
  val *= val;
  return val * val;
}

vec2 getDistance(vec3 o, vec3 l, float r) {
  float a = -dot(l, o);
  float d = a * a - dot(o, o) + r * r;
  if (d < 0) {
    return vec2(-1, -1);
  }

  float dsqrt = sqrt(d);

  // We are inside the atmosphere, return the distance to the far intersection
  return vec2(a + dsqrt, a - dsqrt);
}

vec2 getTransmittanceUV(float r, float mu) {
  float uR, uMu;
  uR = sqrt((r - Rg) / (Rt - Rg));
  uMu = atan((mu + 0.15) / (1.0 + 0.15) * tan(1.5)) / 1.5;
  return vec2(uMu, uR);
}

// transmittance(=transparency) of atmosphere for infinite ray (r,mu)
// (mu=cos(view zenith angle)), intersections with ground ignored
vec3 transmittance(float r, float mu) {
	  vec2 uv = getTransmittanceUV(r, mu);
    return texture2D(uTexture, uv).rgb;
}

// transmittance(=transparency) of atmosphere for infinite ray (r,mu)
// (mu=cos(view zenith angle)), or zero if ray intersects ground
vec3 transmittanceWithShadow(float r, float mu) {
    return mu < -sqrt(1.0 - (Rg / r) * (Rg / r)) ? vec3(0.0) : transmittance(r, mu);
}

// transmittance(=transparency) of atmosphere between x and x0
// assume segment x,x0 not intersecting ground
// r=||x||, mu=cos(zenith angle of [x,x0) ray at x), v=unit direction vector of [x,x0) ray
vec3 transmittance(float r, float mu, vec3 v, vec3 x0) {
    vec3 result;
    float r1 = length(x0);
    float mu1 = dot(x0, v) / r;
    if (mu > 0.0) {
        result = min(transmittance(r, mu) / transmittance(r1, mu1), 1.0);
    } else {
        result = min(transmittance(r1, -mu1) / transmittance(r, -mu), 1.0);
    }
    return result;
}

vec4 texture4D(sampler3D table, float r, float mu, float muS, float nu) {
    float H = sqrt(Rt * Rt - Rg * Rg);
    float rho = sqrt(r * r - Rg * Rg);

    float rmu = r * mu;
    float delta = rmu * rmu - r * r + Rg * Rg;
    vec4 cst = rmu < 0.0 && delta > 0.0 ? vec4(1.0, 0.0, 0.0, 0.5 - 0.5 / float(RES_MU)) : vec4(-1.0, H * H, H, 0.5 + 0.5 / float(RES_MU));
	  float uR = 0.5 / float(RES_R) + rho / H * (1.0 - 1.0 / float(RES_R));
    float uMu = cst.w + (rmu * cst.x + sqrt(delta + cst.y)) / (rho + cst.z) * (0.5 - 1.0 / float(RES_MU));
    // paper formula
    //float uMuS = 0.5 / float(RES_MU_S) + max((1.0 - exp(-3.0 * muS - 0.6)) / (1.0 - exp(-3.6)), 0.0) * (1.0 - 1.0 / float(RES_MU_S));
    // better formula
    float uMuS = 0.5 / float(RES_MU_S) + (atan(max(muS, -0.1975) * tan(1.26 * 1.1)) / 1.1 + (1.0 - 0.26)) * 0.5 * (1.0 - 1.0 / float(RES_MU_S));

    float lerp = (nu + 1.0) / 2.0 * (float(RES_NU) - 1.0);
    float uNu = floor(lerp);
    lerp = lerp - uNu;
    return texture(table, vec3((uNu + uMuS) / float(RES_NU), uMu, uR)) * (1.0 - lerp) +
           texture(table, vec3((uNu + uMuS + 1.0) / float(RES_NU), uMu, uR)) * lerp;
}

// Rayleigh phase function
float phaseFunctionR(float mu) {
    return (3.0 / (16.0 * M_PI)) * (1.0 + mu * mu);
}

// Mie phase function
float phaseFunctionM(float mu) {
	return 1.5 * 1.0 / (4.0 * M_PI) * (1.0 - mieG*mieG) * pow(1.0 + (mieG*mieG) - 2.0*mieG*mu, -3.0/2.0) * (1.0 + mu * mu) / (2.0 + mieG*mieG);
}

// approximated single Mie scattering (cf. approximate Cm in paragraph "Angular precision")
vec3 getMie(vec4 rayMie) { // rayMie.rgb=C*, rayMie.w=Cm,r
	return rayMie.rgb * rayMie.w / max(rayMie.r, 1e-4) * (betaR.r / betaR);
}

vec3 inscatter(inout vec3 x, vec3 x0, inout float t, vec3 v, vec3 s, out float r, out float mu, out vec3 attenuation) {
    vec3 result;
    r = length(x);
    mu = dot(x, v) / r;
    float d = -r * mu - sqrt(r * r * (mu * mu - 1.0) + Rt * Rt);
    if (d > 0.0) { // if x in space and ray intersects atmosphere
        // move x to nearest intersection of ray with top atmosphere boundary
        x += d * v;
        t -= d;
        mu = (r * mu + d) / Rt;
        r = Rt;
    }
    
    if (r <= Rt) { // if ray intersects atmosphere
        float nu = dot(v, s);
        float muS = dot(x, s) / r;
        float phaseR = phaseFunctionR(nu);
        float phaseM = phaseFunctionM(nu);
        vec4 inscatter = max(texture4D(uSpecularSmoothnessMap, r, mu, muS, nu), 0.0);
        float r0 = length(x0);
        float rMu0 = dot(x0, v);
        float mu0 = rMu0 / r0;
        float muS0 = dot(x0, s) / r0;

        attenuation = transmittance(r, mu, v, x0);
            
        if (r0 < Rt) {
            if (r0 > Rg + 0.01) {
                // computes S[L]-T(x,x0)S[L]|x0
                inscatter = max(inscatter - attenuation.rgbr * texture4D(uSpecularSmoothnessMap, r0, mu0, muS0, nu), 0.0);
            }
        }
        result = max(inscatter.rgb * phaseR + getMie(inscatter) * phaseM, 0.0);
    } else { // x in space and ray looking in space
        result = vec3(0.0);
        attenuation = vec3(1.0);
    }
    return result * ISun;
}

vec4 color() {
  vec2 screenTexCoord = gl_FragCoord.xy*uOneOverScreenSize;
  float depth = texture(uSamplerDepth, screenTexCoord).r;
  vec3 viewDir = normalize(vPosition - uCameraPosition);
    
  float planetRadius = uEmissiveColor.r; //6360;
  float atmosphereRadius = uEmissiveColor.g; //6778;
  
  vec3 camSpaceWorldPos = depth != 1 ? 
      unpackWorldPosition(depth) - uCameraPosition : 
      intersectSphereMax(uCameraPosition, viewDir, uObjectPosition, atmosphereRadius) - uCameraPosition;  

  Rg = planetRadius;
  Rt = atmosphereRadius;
  vec3 worldPos = camSpaceWorldPos - uObjectPosition  + uCameraPosition;
   

  vec3 camSpaceAtmospherePos = intersectSphere(uCameraPosition, viewDir, uObjectPosition, atmosphereRadius) - uCameraPosition;
  
  if(camSpaceAtmospherePos == -uCameraPosition) discard;



  float atmosphereThickness = atmosphereRadius - planetRadius;
  
  vec3 atmospherePos = camSpaceAtmospherePos - uObjectPosition;
  
  vec3 currentPos = camSpaceAtmospherePos;
  
  float totalDistance = depth == 1 ? 1e37 : abs(length(camSpaceWorldPos) - length(camSpaceAtmospherePos));
  totalDistance = min(totalDistance, length(camSpaceAtmospherePos - vPosition + uCameraPosition));
  
  int steps = 30;
  
  float sunAmount = max( dot( viewDir, uSunDir ), 0.0 );
  float distance = totalDistance/steps;
  float totalFog = 0;
  
  float fogAmount;
  vec3 fogColor;
  
  float shadowTerm = 0;
  
  vec3 localPos = currentPos - uObjectPosition + uCameraPosition;
  

  vec3 x = localPos;
  vec3 v = normalize(viewDir);

  float r = length(x);
  
  if(r < Rg) {
      localPos = localPos * Rg/r;
      r = Rg;
  }
  
  float mu = dot(x, v) / r;
  
  float t = length(localPos - worldPos);
  


  vec3 g = x - vec3(0.0, 0.0, planetRadius + 10.0);
  float a = v.x * v.x + v.y * v.y - v.z * v.z;
  float b = 2.0 * (g.x * v.x + g.y * v.y - g.z * v.z);
  float c = g.x * g.x + g.y * g.y - g.z * g.z;
  float d = -(b + sqrt(b * b - 4.0 * a * c)) / (2.0 * a);
  bool cone = d > 0.0 && abs(x.z + d * v.z - planetRadius) <= 10.0;

  if (t > 0.0) {
    if (cone && d < t) {
      t = d;
    }
  } else if (cone) {
    t = d;
  }

  vec3 attenuation = vec3(1.0);
  vec3 s = uSunDir;
  fogColor =  max(vec3(0.0), inscatter(x, worldPos, t, v, s, r, mu, attenuation));
  
  if(length(attenuation) < 0.5) attenuation = vec3(1);

  vec3 backgroundColor = texture(uSamplerColor, screenTexCoord).rgb;
  fogColor += max(vec3(0.0), backgroundColor);
  
  fogAmount = 1.0;
      
  
  return vec4(fogColor, fogAmount); //texture(uTexture, teTexCoord).rgba * uTintColor;
}

vec4 emissive() {
  return uEmissiveColor;
}

vec3 normal() {
  return vNormal;
}
