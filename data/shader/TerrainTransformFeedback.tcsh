#version 410 core

#pragma import "noise/noise3d.glsl"
#pragma import "Utils.glsl"
#pragma import "planets/earth/earth_texture.glsl"

layout(vertices = 3) out;

in vec3 vPosition[];
in vec2 vTexCoord[];
in vec3 vNormal[];

out vec3 tcNormal[];
out vec2 tcTexCoord[];
out vec3 tcPosition[];

uniform mat4 uModelMatrix;
uniform vec3 cameraPosition;
uniform float recursionDepth;
uniform vec2 viewport;

#define ID gl_InvocationID


float getTessLevel(float d0, float d1){
    float avg = (d0 + d1) / 2.0;
    float factor = 1.0/(recursionDepth*recursionDepth);
   
    if(avg >= 2000 * factor){
   		return 1;
   	}
   	if(avg >= 1800 * factor){
   		return 2;
   	}
   	if(avg >= 1000 * factor){
   		return 4;
   	}
   	if(avg >= 400 * factor){
   		return 8;
   	}
   	if(avg >= 200 * factor){
   		return 16;
   	}
   	if(avg >= 100 * factor){
   		return 32;
   	}
   	if(avg >= 0){
   		return 64;
   	}
   	else return 1;
}


void main(){

	tcNormal[ID] = vNormal[ID];
	tcTexCoord[ID] = vTexCoord[ID];
	tcPosition[ID] = vPosition[ID];

	if (ID == 0) {

		vec4 p0 = vec4(texture_getDisplacedPosition_modelspace(normalize(tcPosition[0]), normalize(tcNormal[0])), 1);
		vec4 p1 = vec4(texture_getDisplacedPosition_modelspace(normalize(tcPosition[1]), normalize(tcNormal[1])), 1);
		vec4 p2 = vec4(texture_getDisplacedPosition_modelspace(normalize(tcPosition[2]), normalize(tcNormal[2])), 1);

		float d0 = distance(cameraPosition, (uModelMatrix * p0).xyz );
	    float d1 = distance(cameraPosition, (uModelMatrix * p1).xyz );
	    float d2 = distance(cameraPosition, (uModelMatrix * p2).xyz );

		gl_TessLevelOuter[0] = getTessLevel(d1, d2);
	    gl_TessLevelOuter[1] = getTessLevel(d2, d0);
	    gl_TessLevelOuter[2] = getTessLevel(d0, d1);
	    gl_TessLevelInner[0] = 0.5 * gl_TessLevelOuter[2];

	    /*

	    float avgDist = (d0 + d1 + d2)/3.0;

	    vec3 midPoint;
	    float fDistance;

	    midPoint = (tcPosition[2] + tcPosition[0] / 2.0);
	    fDistance = distance(midPoint, cameraPosition) * 1;
	    gl_TessLevelOuter[0] = getTessLevel(fDistance, fDistance);


	   	midPoint = (tcPosition[2] + tcPosition[0] / 2.0);
	    fDistance = distance(midPoint, cameraPosition) * 1;
	    gl_TessLevelOuter[1] = getTessLevel(fDistance, fDistance);


	    midPoint = (tcPosition[2] + tcPosition[0] / 2.0);
	    fDistance = distance(midPoint, cameraPosition) * 1;
	    gl_TessLevelOuter[2] = getTessLevel(fDistance, fDistance);

	    gl_TessLevelInner[0] = (gl_TessLevelOuter[0] + gl_TessLevelOuter[1] +gl_TessLevelOuter[2]) /3;
	    */
	}
}
