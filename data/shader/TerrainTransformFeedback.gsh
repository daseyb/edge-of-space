#version 410 core

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in vec3 tPosition[3];

in vec3 tNormal[3];
in vec2 tTexCoord[3];

out vec3 gNormal;
out vec3 gPosition;
out vec2 gTexCoord;


void main()
{
    gNormal = tNormal[0];
    gTexCoord = tTexCoord[0];
    gPosition = tPosition[0];

    gl_Position = gl_in[0].gl_Position; EmitVertex();

    gNormal = tNormal[1];
    gTexCoord = tTexCoord[1];
    gPosition = tPosition[1];

    gl_Position = gl_in[1].gl_Position; EmitVertex();

    gNormal = tNormal[2];
    gTexCoord = tTexCoord[2];
    gPosition = tPosition[2];

    gl_Position = gl_in[2].gl_Position; EmitVertex();

    EndPrimitive();
}
