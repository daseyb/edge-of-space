//================================ NORMAL APROXIMATION: ==========0


#define SAMPLE_ANGLE__NORMAL_VECTOR 0.000005


//forward declarations, for the case that one of the functions is not implemented/the according approxNormal funciton not used
vec3 texture_getDisplacedPosition_modelspace(vec3 normalizedPosInModelspace, vec3 normalizedNormal);
vec3 texture_water_getDisplacedPosition_modelspace(vec3 normalizedPosInModelspace, vec3 normalizedNormal);




//================================
// function taken from:
// http://www.neilmendoza.com/glsl-rotation-about-an-arbitrary-axis/
// (14.12.2015)
//================================
// The following functions are just needed for calculating the modified normal-vectors of the z-displaced vertex;
// When calculating the normals in a smarter way, they may not be needed anymore..
//================================

mat4 rotationMatrix4d(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;

    return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
                0.0,                                0.0,                                0.0,                                1.0);
}

//modified version to get 3d-matrix:
mat3 rotationMatrix3d(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;

    return mat3(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c           );
}
//================================

//================================
// Returns a vector orthogonal tu the given one. (used for normal-vector calculation)
vec3 calculateOrthogonalVector(vec3 originalVector) {
    if (originalVector.z == 0) {
        return vec3(0., 0., 1.);
    } else {
        return vec3(1., 0., -originalVector.x / originalVector.z);
    }
}

vec3 approximateTerrainNormal(vec3 position_inModelspace) {
    // We will use small rotations around axises perpendicular to the given positionvector.
    // For that, an approximation of the angle is calculated to get similar behaviour for planets of different radius
    float angle = SAMPLE_ANGLE__NORMAL_VECTOR / length(position_inModelspace);

    // Now get the rotation-axises and matrices:
    vec3 axisA = calculateOrthogonalVector(position_inModelspace);
    vec3 axisB = cross(position_inModelspace, axisA);

    mat3 rotateA1 = rotationMatrix3d(axisA, angle);
    mat3 rotateA2 = rotationMatrix3d(axisA, -angle);
    mat3 rotateB1 = rotationMatrix3d(axisB, angle);
    mat3 rotateB2 = rotationMatrix3d(axisB, -angle);

    //calculate the points to sample the noisefunction at:
    vec3 virtVertexA1 = rotateA1 * position_inModelspace;
    vec3 virtVertexA2 = rotateA2 * position_inModelspace;
    vec3 virtVertexB1 = rotateB1 * position_inModelspace;
    vec3 virtVertexB2 = rotateB2 * position_inModelspace;

    //calculate points with modified "Z"-value:
    virtVertexA1 = texture_getDisplacedPosition_modelspace(normalize(virtVertexA1), normalize(virtVertexA1));
    virtVertexA2 = texture_getDisplacedPosition_modelspace(normalize(virtVertexA2), normalize(virtVertexA2));
    virtVertexB1 = texture_getDisplacedPosition_modelspace(normalize(virtVertexB1), normalize(virtVertexB1));
    virtVertexB2 = texture_getDisplacedPosition_modelspace(normalize(virtVertexB2), normalize(virtVertexB2));
//    virtVertexA1 = calculateDisplacedVertex_InModelSpace_complete(virtVertexA1, worldScale);
//    virtVertexA2 = calculateDisplacedVertex_InModelSpace_complete(virtVertexA2, worldScale);
//    virtVertexB1 = calculateDisplacedVertex_InModelSpace_complete(virtVertexB1, worldScale);
//    virtVertexB2 = calculateDisplacedVertex_InModelSpace_complete(virtVertexB2, worldScale);

    //calculate new normal (may point into wrong direction):
    vec3 normal = normalize(cross(virtVertexA1 - virtVertexA2, virtVertexB1 - virtVertexB2));

    //check if normal points into the correct direction, invert otherwise:
    if (dot(normal, position_inModelspace) < 0.) {
        normal = -normal;
    }

    return normal;
}


vec3 approximateWaterNormal(vec3 position_inModelspace) {
    // We will use small rotations around axises perpendicular to the given positionvector.
    // For that, an approximation of the angle is calculated to get similar behaviour for planets of different radius
    float angle = SAMPLE_ANGLE__NORMAL_VECTOR / length(position_inModelspace);

    // Now get the rotation-axises and matrices:
    vec3 axisA = calculateOrthogonalVector(position_inModelspace);
    vec3 axisB = cross(position_inModelspace, axisA);

    mat3 rotateA1 = rotationMatrix3d(axisA, angle);
    mat3 rotateA2 = rotationMatrix3d(axisA, -angle);
    mat3 rotateB1 = rotationMatrix3d(axisB, angle);
    mat3 rotateB2 = rotationMatrix3d(axisB, -angle);

    //calculate the points to sample the noisefunction at:
    vec3 virtVertexA1 = rotateA1 * position_inModelspace;
    vec3 virtVertexA2 = rotateA2 * position_inModelspace;
    vec3 virtVertexB1 = rotateB1 * position_inModelspace;
    vec3 virtVertexB2 = rotateB2 * position_inModelspace;

    //calculate points with modified "Z"-value:
    virtVertexA1 = texture_water_getDisplacedPosition_modelspace(normalize(virtVertexA1), normalize(virtVertexA1));
    virtVertexA2 = texture_water_getDisplacedPosition_modelspace(normalize(virtVertexA2), normalize(virtVertexA2));
    virtVertexB1 = texture_water_getDisplacedPosition_modelspace(normalize(virtVertexB1), normalize(virtVertexB1));
    virtVertexB2 = texture_water_getDisplacedPosition_modelspace(normalize(virtVertexB2), normalize(virtVertexB2));

    //calculate new normal (may point into wrong direction):
    vec3 normal = normalize(cross(virtVertexA1 - virtVertexA2, virtVertexB1 - virtVertexB2));

    //check if normal points into the correct direction, invert otherwise:
    if (dot(normal, position_inModelspace) < 0.) {
        normal = -normal;
    }

    return normal;
}
