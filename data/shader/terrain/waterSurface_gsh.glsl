layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in vec3 tPosition[3];
in vec3 tPatchDistance[3];

in vec3 tNormal[3];
in vec2 tTexCoord[3];

in vec3 tPosInModelspace[3];

out vec3 gNormal;
out vec3 gPosition;
out vec2 gTexCoord;

out vec3 gPatchDistance;
out vec3 gTriDistance;

out vec3 gPosInModelspace;

void main()
{
    gPatchDistance = tPatchDistance[0];
    gNormal = tNormal[0];
    gTexCoord = tTexCoord[0];
    gPosition = tPosition[0];
    gPosInModelspace = tPosInModelspace[0];

    gTriDistance = vec3(1, 0, 0);
    gl_Position = gl_in[0].gl_Position; EmitVertex();

    gNormal = tNormal[1];
    gTexCoord = tTexCoord[1];
    gPosition = tPosition[1];
    gPosInModelspace = tPosInModelspace[1];

    gPatchDistance = tPatchDistance[1];
    gTriDistance = vec3(0, 1, 0);
    gl_Position = gl_in[1].gl_Position; EmitVertex();

    gNormal = tNormal[2];
    gTexCoord = tTexCoord[2];
    gPosition = tPosition[2];
    gPosInModelspace = tPosInModelspace[2];

    gPatchDistance = tPatchDistance[2];
    gTriDistance = vec3(0, 0, 1);
    gl_Position = gl_in[2].gl_Position; EmitVertex();

    EndPrimitive();
}
