
#pragma import "normalApproximation.glsl"

in vec3 gPosInModelspace;

void initShader(){};

vec4 color() {
  vec4 color = texture(uTexture, gTexCoord).rgba * uTintColor;
  color.a = 1;

  if (texture_getFinalHeight(gPosInModelspace) > texture_water_getWaterLevelHeightAt(gPosInModelspace)) {
    vec3 approxNormal = approximateTerrainNormal(gPosInModelspace);
    color = texture_getColor(gPosInModelspace, gNormal, approxNormal); //gNormal assumed to be a normalized upvector here!
  }

  return color;
}

vec4 emissive() {
  return uEmissiveColor;
}

vec3 normal() {
  return gNormal;
}

vec4 specularSmoothness() {
    return vec4(0.4, 0.4, 0.8, 0.85);
}
