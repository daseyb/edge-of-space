#version 410 core

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in vec3 tPosition[3];
in vec3 tPatchDistance[3];

in vec3 tNormal[3];
in vec2 tTexCoord[3];

//in float tHeight[3];
//in vec3 tUpVector_normalized[3];
in vec3 tPosInModelspace[3];

out vec3 gNormal;
out vec3 gPosition;
out vec2 gTexCoord;

//out float gHeight;
//out float gSurfaceAngle;
//out vec3 gUpVector_normalized;
out vec3 gPosInModelspace;

out vec3 gPatchDistance;
out vec3 gTriDistance;

void main()
{
    //vec3 A = tPosition[2] - tPosition[0];
    //vec3 B = tPosition[1] - tPosition[0];
    //gNormal = -normalize(cross(A, B));

    gPatchDistance = tPatchDistance[0];
    
    gTexCoord = tTexCoord[0];
    gPosition = tPosition[0];

    //gHeight = tHeight[0];
    //gSurfaceAngle = acos(dot(gNormal, tUpVector[0]));
    //gUpVector_normalized = tUpVector_normalized[0];
    gPosInModelspace = tPosInModelspace[0];
    
    gNormal = tNormal[0];
    gTriDistance = vec3(1, 0, 0);
    gl_Position = gl_in[0].gl_Position; EmitVertex();

    gTexCoord = tTexCoord[1];
    gPosition = tPosition[1];

    //gHeight = tHeight[1];
    //gSurfaceAngle = acos(dot(gNormal, tUpVector[1]));
    //gUpVector_normalized = tUpVector_normalized[1];
    gPosInModelspace = tPosInModelspace[1];
    
    gNormal = tNormal[1];
    gPatchDistance = tPatchDistance[1];
    gTriDistance = vec3(0, 1, 0);
    gl_Position = gl_in[1].gl_Position; EmitVertex();

    gTexCoord = tTexCoord[2];
    gPosition = tPosition[2];

    //gHeight = tHeight[2];
    //gSurfaceAngle = acos(dot(gNormal, tUpVector[2]));
    //gUpVector_normalized = tUpVector_normalized[2];
    gPosInModelspace = tPosInModelspace[2];
    
    gNormal = tNormal[2];
    gPatchDistance = tPatchDistance[2];
    gTriDistance = vec3(0, 0, 1);
    gl_Position = gl_in[2].gl_Position; EmitVertex();

    EndPrimitive();
}
