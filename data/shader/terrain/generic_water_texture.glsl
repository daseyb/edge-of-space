
float texture_water_getRidgedDetailsHeightNoise(vec3 normalizedPosInModelspace) {
    return (.75 - abs(fbm_3d( vec3(normalizedPosInModelspace +vec3( uTime* 0.001)) , 3, 320.))) * 0.0003;
    //return 0.;
}

float texture_water_getWaterLevelHeightAt(vec3 normalizedPosInModelspace) {
    return texture_water_getRidgedDetailsHeightNoise(normalizedPosInModelspace);
}


//float texture_water_getWaterLevelHeightAt(vec3 normalizedPosInModelspace) {
//    return sin(75 * 2 * M_PI * /*uTime*/1. * normalizedPosInModelspace.x * normalizedPosInModelspace.y * normalizedPosInModelspace.z) * 0.000025;
//}

vec3 texture_water_getDisplacedPosition_modelspace(vec3 normalizedPosInModelspace, vec3 normalizedNormal) {
    vec3 newPosition = normalizedPosInModelspace + normalizedNormal * texture_water_getWaterLevelHeightAt(normalizedPosInModelspace);
    return newPosition;
}


