#pragma import "normalApproximation.glsl"

//in vec3 gUpVector_normalized;
in vec3 gPosInModelspace;


vec3 approxNormal;


void initShader(){};

float amplify(float d, float scale, float offset){
  d = scale * d + offset;
  d = clamp(d, 0, 1);
  d = 1 - exp2(-2*d*d);
  return d;
}

vec4 color() {
  approxNormal = approximateTerrainNormal(normalize(gPosInModelspace));
  //approxNormal = inNormal;

  vec4 color = texture_getColor(gPosInModelspace, gNormal, approxNormal, length(cameraPosition - inPosition));

  /*float d1 = min(min(gTriDistance.x, gTriDistance.y), gTriDistance.z);
  float d2 = min(min(gPatchDistance.x, gPatchDistance.y), gPatchDistance.z);
  color = amplify(d1, 80, -0.5) * amplify(d2, 120, -0.5) * color;*/
  color.a = 1.0;
  return color;
}

vec4 emissive() {
  return uEmissiveColor;
}

vec3 normal() {
  return approxNormal;
}

vec4 specularSmoothness() {
    return vec4(0.2, 0.2, 0.2, 0.1);
}
