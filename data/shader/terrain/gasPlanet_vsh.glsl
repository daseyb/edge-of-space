#pragma import "../CommonDeferredVert.glsl"

out vec3 vPosInModelspace;

vec3 normal() {
  return aPosition;
}

vec2 texCoord() {
  return aTexCoord;
}

vec3 position() {
    vPosInModelspace = aPosition;

    vec4 modelPos = vec4(aPosition, 1.0);
    vec4 worldPosition = uModelMatrix * modelPos;
    return worldPosition.xyz;
}
