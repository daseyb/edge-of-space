
#pragma import "normalApproximation.glsl"

//in vec3 gUpVector_normalized;
in vec3 gPosInModelspace;


vec3 approxNormal;
bool isWater;
vec3 pos;

void initShader() {
    pos =  gPosInModelspace;
    if (texture_getFinalHeight(pos) < texture_water_getWaterLevelHeightAt(pos)) {
        //water visible:
        isWater = true;
        approxNormal = approximateWaterNormal(normalize(pos));

    } else {
        //land visible:
        isWater = false;
        approxNormal = approximateTerrainNormal(normalize(pos));

    }
    
     approxNormal = normalize(approxNormal);
}

float amplify(float d, float scale, float offset){
  d = scale * d + offset;
  d = clamp(d, 0, 1);
  d = 1 - exp2(-2*d*d);
  return d;
}

vec4 color() {
  //approxNormal = inNormal;

  vec4 color = vec4(1.);

  if (isWater) {
      color = vec4(0.1, 0.2, 1., 1.);
  } else {
      color = texture_getColor(pos, gNormal, approxNormal);
  }

  color.a = 1.0;
  return color;
}

vec4 emissive() {
  //return uEmissiveColor;
  if (isWater) {
      return vec4(0., 0., 0.001, 1.);
  } else {
      return vec4(0., 0., 0., 1.);
  }
}

vec3 normal() {
  return approxNormal;
}

vec4 specularSmoothness() {
    if (isWater) {
        return vec4(0.4, 0.4, 0.8, .85);
    } else {
        return vec4(0.2, 0.2, 0.2, 0.1);
    }
}
