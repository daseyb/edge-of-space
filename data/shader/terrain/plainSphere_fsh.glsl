in vec3 gPosInModelspace;

void initShader(){};


vec4 color() {

  vec4 color = texture_getColor(gPosInModelspace, inNormal, inNormal, length(cameraPosition - inPosition));
  color.a = 1.0;
  return color;
}

vec4 emissive() {
  return uEmissiveColor;
}

vec3 normal() {
  return inNormal;
}

vec4 specularSmoothness() {
    return vec4(0.2, 0.2, 0.2, 0.1);
}
