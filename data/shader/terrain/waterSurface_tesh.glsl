layout(triangles, equal_spacing, ccw) in;

in vec2 tcTexCoord[];
in vec3 tcNormal[];
in vec3 tcPosition[];

out vec3 tNormal;
out vec2 tTexCoord;
out vec3 tPosition;

out vec3 tPatchDistance;

out vec3 tPosInModelspace; // for calculating terrainheight (in fragshader), so that it can be compared to waterheight for better lod-smoothness

uniform mat4 uViewProjectionMatrix;
uniform mat4 uModelMatrix;
//uniform float uTime;

vec2 interpolate2D(vec2 v0, vec2 v1, vec2 v2){
  return vec2(gl_TessCoord.x) * v0 + vec2(gl_TessCoord.y) * v1 + vec2(gl_TessCoord.z) * v2;
}

vec3 interpolate3D(vec3 v0, vec3 v1, vec3 v2){
  return vec3(gl_TessCoord.x ) * v0 + vec3(gl_TessCoord.y) * v1 + vec3(gl_TessCoord.z) * v2;
}


void main(){

  tNormal = interpolate3D(tcNormal[0], tcNormal[1], tcNormal[2]);
  tTexCoord = interpolate2D(tcTexCoord[0], tcTexCoord[1], tcTexCoord[2]);
  tNormal = normalize(tNormal);

  tPatchDistance = gl_TessCoord;
  tPosition = interpolate3D(tcPosition[0], tcPosition[1], tcPosition[2]);
  tPosition = normalize(tPosition);
  tPosInModelspace = tPosition;

  tPosition = texture_water_getDisplacedPosition_modelspace(tPosition, tNormal);
  //tPosition += tNormal * texture_water_getWaterLevelHeightAt(tPosition);

  tPosition = (uModelMatrix * vec4(tPosition, 1)).xyz;

  gl_Position = uViewProjectionMatrix * vec4(tPosition, 1);

}
