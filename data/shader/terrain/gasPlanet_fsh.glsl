
in vec3 vPosInModelspace;

vec3 approxNormal;

void initShader(){
    approxNormal = inNormal;
};

vec4 color() {

  vec4 color = texture_getColor(vPosInModelspace, approxNormal, approxNormal, length(cameraPosition - inPosition));

  color.a = 1.0;

  return vec4(1.,0.,0.,1.);
  return color;
}

vec4 emissive() {
  return uEmissiveColor;
}

vec3 normal() {
  return approxNormal;
}

vec4 specularSmoothness() {
    return vec4(0.2, 0.2, 0.2, 0.1);
}
