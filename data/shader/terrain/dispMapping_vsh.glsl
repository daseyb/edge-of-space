in vec3 gNormal;
in vec3 gPosition;
in vec2 gTexCoord;

out vec3 vNormal;
out vec3 vPosition;
out vec2 vTexCoord;

void main(){
  vNormal = gNormal;
  vPosition = gPosition;
  vTexCoord = gTexCoord;
}
