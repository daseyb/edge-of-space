#version 410 core

layout(vertices = 3) out;

in vec3 vNormal[];
in vec3 vPosition[];
in vec2 vTexCoord[];

out vec3 tcNormal[];
out vec2 tcTexCoord[];
out vec3 tcPosition[];

uniform mat4 uModelMatrix;

uniform vec3 cameraPosition;

#define ID gl_InvocationID


float getTessLevel(float d0, float d1){

    float avg = (d0 + d1) / 2.0;
    if(avg >= 100){
                return 1;
        }
        if(avg >= 80){
                return 2;
        }
        if(avg >= 60){
                return 4;
        }
        if(avg >= 40){
                return 8;
        }
        if(avg >= 20){
                return 16;
        }
        if(avg >= 10){
                return 32;
        }
        if(avg >= 0){
                return 64;
        }
        else return 1;
}


void main(){

        tcNormal[ID] = vNormal[ID];
        tcTexCoord[ID] = vTexCoord[ID];
        tcPosition[ID] = vPosition[ID];

        if (ID == 0) {



                float d0 = distance(cameraPosition, (uModelMatrix * vec4(tcPosition[0], 1)).xyz );
            float d1 = distance(cameraPosition, (uModelMatrix * vec4(tcPosition[1], 1)).xyz );
            float d2 = distance(cameraPosition, (uModelMatrix * vec4(tcPosition[2], 1)).xyz );

            gl_TessLevelOuter[0] = getTessLevel(d1, d2);
            gl_TessLevelOuter[1] = getTessLevel(d2, d0);
            gl_TessLevelOuter[2] = getTessLevel(d0, d1);
            gl_TessLevelInner[0] = 0.5 * gl_TessLevelOuter[2];

            /*

            float avgDist = (d0 + d1 + d2)/3.0;

            vec3 midPoint;
            float fDistance;

            midPoint = (tcPosition[2] + tcPosition[0] / 2.0);
            fDistance = distance(midPoint, cameraPosition) * 1;
            gl_TessLevelOuter[0] = getTessLevel(fDistance, fDistance);


                midPoint = (tcPosition[2] + tcPosition[0] / 2.0);
            fDistance = distance(midPoint, cameraPosition) * 1;
            gl_TessLevelOuter[1] = getTessLevel(fDistance, fDistance);


            midPoint = (tcPosition[2] + tcPosition[0] / 2.0);
            fDistance = distance(midPoint, cameraPosition) * 1;
            gl_TessLevelOuter[2] = getTessLevel(fDistance, fDistance);

            gl_TessLevelInner[0] = (gl_TessLevelOuter[0] + gl_TessLevelOuter[1] +gl_TessLevelOuter[2]) /3;
            */
        }
}
