float getContinentHeightNoise(vec3 normalizedPosInModelspace) {
    float val = 2*(fbm_3d(normalizedPosInModelspace*2 , 4, 1.25)) + 0.1;
    return val;
}
float getRidgedHeightNoise(vec3 normalizedPosInModelspace) {
    return (.75 - abs(fbm_3d( normalizedPosInModelspace , 2, 40.)))/2.;
}
float getDetailHeightNoise(vec3 normalizedPosInModelspace) {
    return fbm_3d( normalizedPosInModelspace , 2, 160.)/8.;
}
float getRidgedDetailsHeightNoise(vec3 normalizedPosInModelspace) {
    return (.75 - abs(fbm_3d( normalizedPosInModelspace , 4, 8*640.)))/256.;
    //return 0.;
}

float getHeightNoise(vec3 normalizedPosInModelspace) {
    float continent = getContinentHeightNoise(normalizedPosInModelspace);
    return ( continent * 0.3 + 
            + getRidgedHeightNoise(normalizedPosInModelspace) * 2 * continent
            + (getDetailHeightNoise(normalizedPosInModelspace) + 0.1) * (1.0 - pow(1.0 - continent, 3)) * 2
			+ getDetailHeightNoise(normalizedPosInModelspace * 0.25) * ( pow(0.5 - continent*0.5, 2))
            + getRidgedDetailsHeightNoise(normalizedPosInModelspace) * 0.2)
            * 1.;
}


#define SNOW_LEVEL .6
#define ROCK_LEVEL .3
#define PLANT_LEVEL .03

#define ROCK_SLOPE .6

vec4 texture_getColor(vec3 texCoord3d, vec3 upVectorNormalized, vec3 normalNormalized, float depth){
    //float noiseVal1 = fbm_3d( texCoord3d,4, 128.*128.);// * 0.05;
    float noiseOct1 = 0.;
    float noiseOct2 = 0.;
    float noiseOct3 = 0.;
    float noiseOct4 = 0.;
    float noiseOct5 = 0.;

    noiseOct1 = snoise_3d(32.*128. * texCoord3d);
    noiseOct2 = snoise_3d(64.*128. * texCoord3d);
    noiseOct3 = snoise_3d(128.*128. * texCoord3d);

    float heightPertubation = 1./2.*noiseOct1 + 1./4.*noiseOct2 + 1./8.*noiseOct3;

// just some test:
//        if (noiseOct1 < 0. || noiseOct2 < 0. || noiseOct3 < 0. || noiseOct4 < 0. || noiseOct5 < 0. ) {
//            return vec4 (0.,0.,0.,1.);
//        }

    //vec3 distortion = vec3 (.1, .1, .1);

    vec4 color = vec4(1., 1., 1., 1.);

    //float ridgedHeight = getRidgedHeightNoise(texCoord3d);

//    float height =(
//                getContinentHeightNoise(texCoord3d)
//                + ridgedHeight
//                + getDetailHeightNoise(texCoord3d)
//                )*.9 + heightPertubation * .1;

    float height =getHeightNoise(texCoord3d)/2.*.9 + heightPertubation * 0.05;

    //testing stuff:
    //return vec4(normalNormalized.x,normalNormalized.y, normalNormalized.z, 1.);
    //color = vec4(upVectorNormalized.x,upVectorNormalized.y, upVectorNormalized.z, 1.);

    float dt = dot(normalNormalized, upVectorNormalized);
    float cs = dt / (length(normalNormalized) * length(upVectorNormalized));
    float phi = acos(dt);
    float phiNormalized = 2*phi / M_PI; //scale results from [0, M_PI/2] to [0, 1]

    float phiPertubation = 1./2.*noiseOct4 + 1./4.*noiseOct5;
    float phiPert = .9*phiNormalized + .1 * phiPertubation;


    if (height> SNOW_LEVEL && phiPert <= ROCK_SLOPE) {
        color = vec4(1., 1., 1., 1.) * 0.9;

    } else if (height> ROCK_LEVEL || phiPert > ROCK_SLOPE) {
        color = vec4(.1, .1, .1, 1.);

    } else if (height> PLANT_LEVEL) {
        color = vec4(.2, .5,.15, 1.);
    }else { //"sand""
        color = vec4(1., .8, .55, 1.);
    }
    return color;
}

vec4 texture_getColor(vec3 texCoord3d, vec3 upVectorNormalized, vec3 normalNormalized){
    return texture_getColor(texCoord3d, upVectorNormalized, normalNormalized, 1.);
}

float texture_getFinalHeight(vec3 normalizedPosInModelspace){
    return 0.008 * getHeightNoise(normalizedPosInModelspace);
}


vec3 texture_getDisplacedPosition_modelspace(vec3 normalizedPosInModelspace, vec3 normalizedNormal) {
    vec3 newPosition = normalizedPosInModelspace + normalizedNormal * texture_getFinalHeight(normalizedPosInModelspace);
    return newPosition;
}


