#version 410 core

#pragma import "../../setup/geometry.glsl"

#pragma import "../../noise/noise3d.glsl"
#pragma import "../../noise/noise4d.glsl"

#pragma import "../../Utils.glsl"

#pragma import "../../CommonDeferredFrag.glsl"

#pragma import "mars_texture.glsl"
#pragma import "../../terrain/generic_water_texture.glsl"

#pragma import "../../terrain/dispMappingWithWater_fsh.glsl"
