const float SC = 250.0;

// value noise, and its analytical derivatives


const mat2 m2 = mat2(0.8,-0.6,0.6,0.8);

float getContinentHeightNoise(vec3 normalizedPosInModelspace) {
    return 2*(fbm_3d(normalizedPosInModelspace , 5, 1.25));
}
float getRidgedHeightNoise(vec3 normalizedPosInModelspace) {
    return (.75 - abs(fbm_3d( normalizedPosInModelspace , 2, 40.)))/2.;
}
float getDetailHeightNoise(vec3 normalizedPosInModelspace) {
    return fbm_3d( normalizedPosInModelspace , 2, 160.)/8.;
}
float getRidgedDetailsHeightNoise(vec3 normalizedPosInModelspace) {
    return (.75 - abs(fbm_3d( normalizedPosInModelspace , 6, 640.)))/256.;
    //return 0.;
}

float getLevel(vec3 normalizedPosInModelspace, float count, float power) {
	float levelSample = abs(fbm_3d(normalizedPosInModelspace * 4 +  vec3(0.4, 5, 1) , 2, 3.0) + 1.6) * count;
	float level = floor(levelSample) + pow(fract(levelSample), power);
    level *= 1.0/count;
	return level;
}

float getHeightNoise(vec3 normalizedPosInModelspace) {
    float continent = smoothstep(0.0, 1.0, getContinentHeightNoise(normalizedPosInModelspace));

	float level = getLevel(normalizedPosInModelspace, 4, 2) * 1;

	float result = (floor(continent) + pow(fract(continent), 5)) * 3  +  getDetailHeightNoise(normalizedPosInModelspace) * 0.6  + level;

	result += getRidgedDetailsHeightNoise(normalizedPosInModelspace) * 0.3 + getDetailHeightNoise(normalizedPosInModelspace  * 4) * 0.1;

	return result;
}


#define SNOW_LEVEL .5
#define ROCK_LEVEL .3
#define PLANT_LEVEL .03

#define ROCK_SLOPE .6

vec4 texture_getColor(vec3 texCoord3d, vec3 upVectorNormalized, vec3 normalNormalized, float depth){
    //float noiseVal1 = fbm_3d( texCoord3d,4, 128.*128.);// * 0.05;
    float noiseOct1 = 0.;
    float noiseOct2 = 0.;
    float noiseOct3 = 0.;
    float noiseOct4 = 0.;
    float noiseOct5 = 0.;

    noiseOct1 = snoise_3d(32.*128. * texCoord3d);
    noiseOct2 = snoise_3d(64.*128. * texCoord3d);
    noiseOct3 = snoise_3d(128.*128. * texCoord3d);




    float heightPertubation = 1./2.*noiseOct1 + 1./4.*noiseOct2 + 1./8.*noiseOct3;
    vec4 color = vec4(1., 1., 1., 1.);


    float height =getHeightNoise(texCoord3d)/2.*.9 + heightPertubation * .1;

    float dt = dot(normalNormalized, upVectorNormalized);
    float cs = dt / (length(normalNormalized) * length(upVectorNormalized));
    float phi = acos(dt);
    float phiNormalized = 2*phi / M_PI; //scale results from [0, M_PI/2] to [0, 1]

    float phiPertubation = 1./2.*noiseOct4 + 1./4.*noiseOct5;
    float phiPert = .9*phiNormalized + .1 * phiPertubation;

	color = mix(vec4(1., 0.05, 0.02, 1.), vec4(.9, .1, .1, 1.), height);

	color += vec4(0.1, 0.04, 0.05, 0) * fbm_3d(texCoord3d, 4, 5);

    /*if (height> SNOW_LEVEL && phiPert <= ROCK_SLOPE) {
        color = ;

    } else if (height> ROCK_LEVEL || phiPert > ROCK_SLOPE) {
        color = vec4(.9, .3, .4, 2.);

    } else if (height> PLANT_LEVEL) {
        color = vec4(.2, .5,.15, 1.);
    }else { //"sand""
        color = vec4(1., .8, .55, 1.);
    }*/
    return color;
}

vec4 texture_getColor(vec3 texCoord3d, vec3 upVectorNormalized, vec3 normalNormalized){
    return texture_getColor(texCoord3d, upVectorNormalized, normalNormalized, 1.);
}

float texture_getFinalHeight(vec3 normalizedPosInModelspace){
    return 0.008 * getHeightNoise(normalizedPosInModelspace);
}


vec3 texture_getDisplacedPosition_modelspace(vec3 normalizedPosInModelspace, vec3 normalizedNormal) {
    vec3 newPosition = normalizedPosInModelspace + normalizedNormal * texture_getFinalHeight(normalizedPosInModelspace);
    return newPosition;
}


