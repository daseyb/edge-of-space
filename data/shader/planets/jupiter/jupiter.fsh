#version 410 core

#pragma import "../../setup/geometry.glsl"

#pragma import "../../CommonDeferredFrag.glsl"

#pragma import "jupiter_texture.glsl"

#pragma import "../../terrain/plainSphere_fsh.glsl"
