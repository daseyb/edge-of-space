#pragma import "../../noise/noise3d.glsl"
#pragma import "../../noise/noise2d.glsl"


//float getHeightNoise(vec3 normalizedPosInModelspace) {
//    return (fbm_3d( normalizedPosInModelspace , 4, 80.)/2. + 2*fbm_3d (normalizedPosInModelspace , 2, 1.25));
//}



vec4 texture_getColor(vec3 texCoord3d, vec3 upVectorNormalized, vec3 normalNormalized, float depth){
    float noiseColorRingsR = fbm_2d( vec2(texCoord3d.z, .2),6, 16.);
    float noiseColorRingsG = fbm_2d( vec2(texCoord3d.z, .5),5, 8.);
    float pFlowRings = fbm_2d( vec2(texCoord3d.z, .8),6, 2.);
    float pTime =  (pFlowRings-.5)*2. * uTime*0.05;

    float co= cos(pTime);
    float si= sin(pTime);
    mat3 flowRotation = mat3x3(co , -si, 0.,
                               si, co, 0.,
                               0., 0., 1.);

    texCoord3d = texCoord3d * flowRotation;
    float noiseFlow = fbm_3d( vec3(texCoord3d.xy, uTime*.05),6, 2.);
    return vec4 (noiseFlow * noiseColorRingsR *.7 +.3, noiseFlow * noiseColorRingsG * .8, 0., 1.);

}

//vec4 texture_getColor(vec3 texCoord3d, vec3 upVectorNormalized, vec3 normalNormalized, float depth){
//    //float noiseVal1 = fbm_3d( texCoord3d,4, 128.*128.);// * 0.05;
//    float noiseOct1 = 0.;
//    float noiseOct2 = 0.;
//    float noiseOct3 = 0.;
//    float noiseOct4 = 0.;
//    float noiseOct5 = 0.;

//    if (depth == 1.) {
//        noiseOct1 = snoise_3d(128.*128. * texCoord3d);
//        noiseOct2 = snoise_3d(2*128.*128. * texCoord3d);
//        noiseOct3 = snoise_3d(4*128.*128. * texCoord3d);
//        noiseOct4 = snoise_3d(8*128.*128. * texCoord3d);
//        noiseOct5 = snoise_3d(16*128.*128. * texCoord3d);
//    }

//    vec3 distortion = vec3 (.1, .1, .1);
//    //float noiseVal2 = fbm_3d( texCoord3d, 3, 128.*128.);
//    float noiseVal2 = 1./2.*noiseOct1 + 1./4.*noiseOct2 + 1./8.*noiseOct3;
//    //float noiseVal3 = fbm_3d( texCoord3d + distortion, 4, 8*128.*128.);// * 0.05;
//    float noiseVal3 = 1./2.*noiseOct4 + 1./4.*noiseOct5;
//    vec4 color = vec4(1., 1., 1., 1.);

//    float height = getHeightNoise(texCoord3d)*.8 + noiseVal2 * .2;


//    //color = vec4(normalNormalized.x,normalNormalized.y, normalNormalized.z, 1.);
//    //color = vec4(upVectorNormalized.x,upVectorNormalized.y, upVectorNormalized.z, 1.);
//    float dt = dot(normalNormalized, upVectorNormalized);
//    float cs = dt / (length(normalNormalized) * length(upVectorNormalized));
//    float phi = acos(dt);
//    float phiNormalized = 2*phi / M_PI; //scale results from [0, M_PI/2] to [0, 1]
//    //float c = noiseVal1*.3 +.3;

//    float phiPert = .9*phiNormalized + .1 * noiseVal3;


//    if (height> SNOW_LEVEL && phiPert <= ROCK_SLOPE) {
//        color = vec4(1., 1., 1., 1.);
//    } else if (height> ROCK_LEVEL || phiPert > ROCK_SLOPE) {
//        color = vec4(.1, .1, .1, 1.);

//    } else if (height> PLANT_LEVEL) {
//        color = vec4(.2, .5,.15, 1.);
//    }else { //"sand""
//        color = vec4(1., .8, .55, 1.);
//    }
//    return color;
//}

vec4 texture_getColor(vec3 texCoord3d, vec3 upVectorNormalized, vec3 normalNormalized){
    return texture_getColor(texCoord3d, upVectorNormalized, normalNormalized, 1.);
}

//float texture_getFinalHeight(vec3 normalizedPosInModelspace){
//    return 0.008 * 1.;
//}


//vec3 texture_getDisplacedPosition_modelspace(vec3 normalizedPosInModelspace, vec3 normalizedNormal) {
//    vec3 newPosition = normalizedPosInModelspace + normalizedNormal * texture_getFinalHeight(normalizedPosInModelspace);
//    return newPosition;
//}


