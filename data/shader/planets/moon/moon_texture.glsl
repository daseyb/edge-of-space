float getHeight(vec3 normalizedPosInModelspace) {
    return (fbm_3d( normalizedPosInModelspace , 4, 80.)/2. + 2*fbm_3d (normalizedPosInModelspace , 2, 1.25));
}

vec4 texture_getColor(vec3 texCoord3d, vec3 upVectorNormalized, vec3 normalNormalized, float depth){
    return vec4(.1, .1, .1, .1);
}

vec4 texture_getColor(vec3 texCoord3d, vec3 upVectorNormalized, vec3 normalNormalized){
    return texture_getColor(texCoord3d, upVectorNormalized, normalNormalized, 1.);
}

vec3 texture_getDisplacedPosition_modelspace(vec3 normalizedPosInModelspace, vec3 normalizedNormal) {
    float noiseValue = getHeight(normalizedPosInModelspace);
    vec3 newPosition = normalizedPosInModelspace + normalizedNormal * 0.008 * noiseValue;
    return newPosition;
}
