#pragma import "../noise/noise3d.glsl"

//needed for calculation of modified normal-vector; change until you get a good result for normals:
#define SAMPLE_ANGLE__NORMAL_VECTOR 0.0005
#define M_PI 3.1415926535897932384626433832795

float mountain_getHeight(vec3 normalizedPosInModelspace) {
    return (fbm_3d( normalizedPosInModelspace , 4, 80.)/2. + 2*fbm_3d (normalizedPosInModelspace , 2, 1.25));
}


vec3 calcGrass(float noiseVal1, float noiseVal2, float phiN, float height) {

    return noiseVal1 * vec3(.1, .7, .1);
}

#define SNOW_LEVEL .5
#define ROCK_LEVEL .3
#define PLANT_LEVEL .03

#define ROCK_SLOPE .6

<<<<<<< Updated upstream:data/shader/oldstuff/mountainTexture.glsl
vec4 texture_getColor(vec3 texCoord3d, vec3 upVectorNormalized, vec3 normalNormalized){
=======
vec4 mountainTex_getColor(vec3 texCoord3d, vec3 upVectorNormalized, vec3 normalNormalized, float height){
>>>>>>> Stashed changes:data/shader/terrain/mountainTexture.glsl
    float noiseVal1 = fbm_3d( texCoord3d,4, 128.*128.);// * 0.05;
    vec3 distortion = vec3 (.1, .1, .1);
    float noiseVal2 = fbm_3d( texCoord3d, 4, 8*128.*128.);// * 0.05;
    float noiseVal3 = fbm_3d( texCoord3d + distortion, 4, 8*128.*128.);// * 0.05;
    vec4 color = vec4(1., 1., 1., 1.);

    height = mountain_getHeight(texCoord3d)*.9 + noiseVal2 * .1;


    //color = vec4(normalNormalized.x,normalNormalized.y, normalNormalized.z, 1.);
    //color = vec4(upVectorNormalized.x,upVectorNormalized.y, upVectorNormalized.z, 1.);
    float dt = dot(normalNormalized, upVectorNormalized);
    float cs = dt / (length(normalNormalized) * length(upVectorNormalized));
    float phi = acos(dt);
    float phiNormalized = 2*phi / M_PI; //scale results from [0, M_PI/2] to [0, 1]
    float c = noiseVal1*.3 +.3;

    float phiPert = .9*phiNormalized + .1 * noiseVal3;


    if (height> SNOW_LEVEL && phiPert <= ROCK_SLOPE) {
        color = vec4(1., 1., 1., 1.);
    } else if (height> ROCK_LEVEL || phiPert > ROCK_SLOPE) {
        color = vec4(.1, .1, .1, 1.);

    } else if (height> PLANT_LEVEL) {
        color = vec4(.2, .5,.15, 1.);
    }else { //"sand""
        color = vec4(1., .8, .55, 1.);
    }
    return color;
}




vec3 mountain_getDisplacedPosition_modelspace(vec3 normalizedPosInModelspace, vec3 normalizedNormal) {
    float noiseValue = mountain_getHeight(normalizedPosInModelspace);
    vec3 newPosition = normalizedPosInModelspace + normalizedNormal * 0.008 * noiseValue;
    return newPosition;
}

//================================ NORMAL APROXIMATION: ==========0

//================================
// function taken from:
// http://www.neilmendoza.com/glsl-rotation-about-an-arbitrary-axis/
// (14.12.2015)
//================================
// The following functions are just needed for calculating the modified normal-vectors of the z-displaced vertex;
// When calculating the normals in a smarter way, they may not be needed anymore..
//================================

mat4 rotationMatrix4d(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;

    return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
                0.0,                                0.0,                                0.0,                                1.0);
}

//modified version to get 3d-matrix:
mat3 rotationMatrix3d(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;

    return mat3(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c           );
}
//================================

//================================
// Returns a vector orthogonal tu the given one. (used for normal-vector calculation)
vec3 calculateOrthogonalVector(vec3 originalVector) {
    if (originalVector.z == 0) {
        return vec3(0., 0., 1.);
    } else {
        return vec3(1., 0., -originalVector.x / originalVector.z);
    }
}

vec3 approximateTerrainNormal(vec3 position_inModelspace) {
    // We will use small rotations around axises perpendicular to the given positionvector.
    // For that, an approximation of the angle is calculated to get similar behaviour for planets of different radius
    float angle = SAMPLE_ANGLE__NORMAL_VECTOR / length(position_inModelspace);

    // Now get the rotation-axises and matrices:
    vec3 axisA = calculateOrthogonalVector(position_inModelspace);
    vec3 axisB = cross(position_inModelspace, axisA);

    mat3 rotateA1 = rotationMatrix3d(axisA, angle);
    mat3 rotateA2 = rotationMatrix3d(axisA, -angle);
    mat3 rotateB1 = rotationMatrix3d(axisB, angle);
    mat3 rotateB2 = rotationMatrix3d(axisB, -angle);

    //calculate the points to sample the noisefunction at:
    vec3 virtVertexA1 = rotateA1 * position_inModelspace;
    vec3 virtVertexA2 = rotateA2 * position_inModelspace;
    vec3 virtVertexB1 = rotateB1 * position_inModelspace;
    vec3 virtVertexB2 = rotateB2 * position_inModelspace;

    //calculate points with modified "Z"-value:
    virtVertexA1 = mountain_getDisplacedPosition_modelspace(normalize(virtVertexA1), normalize(virtVertexA1));
    virtVertexA2 = mountain_getDisplacedPosition_modelspace(normalize(virtVertexA2), normalize(virtVertexA2));
    virtVertexB1 = mountain_getDisplacedPosition_modelspace(normalize(virtVertexB1), normalize(virtVertexB1));
    virtVertexB2 = mountain_getDisplacedPosition_modelspace(normalize(virtVertexB2), normalize(virtVertexB2));
//    virtVertexA1 = calculateDisplacedVertex_InModelSpace_complete(virtVertexA1, worldScale);
//    virtVertexA2 = calculateDisplacedVertex_InModelSpace_complete(virtVertexA2, worldScale);
//    virtVertexB1 = calculateDisplacedVertex_InModelSpace_complete(virtVertexB1, worldScale);
//    virtVertexB2 = calculateDisplacedVertex_InModelSpace_complete(virtVertexB2, worldScale);

    //calculate new normal (may point into wrong direction):
    vec3 normal = normalize(cross(virtVertexA1 - virtVertexA2, virtVertexB1 - virtVertexB2));

    //check if normal points into the correct direction, invert otherwise:
    if (dot(normal, position_inModelspace) < 0.) {
        normal = -normal;
    }

    return normal;
}
