#pragma import "../noise/noise3d.glsl"
#pragma import "sphereUV.glsl"

//needed for calculation of modified normal-vector; change until you get a good result for normals:
#define SAMPLE_ANGLE__NORMAL_VECTOR 0.0001



//================================
// function based on:
// https://github.com/ashima/webgl-noise/blob/master/demo/common/noisedemoMain.frag
// (13.12.2015)
//================================
// "coords" defines the vector in UV-space, for which the z-value is to be calculated;
// "scale" is needed for scaling the terrainZ to a value that fits the proportion of your world
float calculateTerrainZ_simple(vec2 coords, float scale) {
    // x, y to be given in range [0., 1.]

    // scale the result (which is in [0., 1.] originally) before return:
    return scale * sumUpOctavesSimple_2d(coords, 6);
}


//================================
// function taken from:
// http://www.neilmendoza.com/glsl-rotation-about-an-arbitrary-axis/
// (14.12.2015)
//================================
// The following functions are just needed for calculating the modified normal-vectors of the z-displaced vertex;
// When calculating the normals in a smarter way, they may not be needed anymore..
//================================

mat4 rotationMatrix4d(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;

    return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
                0.0,                                0.0,                                0.0,                                1.0);
}

//modified version to get 3d-matrix:
mat3 rotationMatrix3d(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;

    return mat3(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c           );
}
//================================

//================================
// Returns a vector orthogonal tu the given one. (used for normal-vector calculation)
vec3 calculateOrthogonalVector(vec3 originalVector) {
    if (originalVector.z == 0) {
        return vec3(0., 0., 1.);
    } else {
        return vec3(1., 0., -originalVector.x / originalVector.z);
    }
}


//================================
//just 3 version of nearly the same function with different parameters:
vec3 calculateDisplacedVertex_InModelSpace_complete(vec3 position_inModelspace, float scale) {
    float u = calculateU(position_inModelspace);
    float v = calculateV(position_inModelspace);

    // calculate Z-displacement:
    float terrainZ = calculateTerrainZ_simple(vec2(u, v), scale);
    float vectorLengthOld = length(position_inModelspace);
    float vectorLengthNew = vectorLengthOld + (terrainZ / vectorLengthOld);
    return vectorLengthNew * position_inModelspace;

}

vec3 calculateDisplacedVertex_InModelSpace_fromUVcoords(vec3 position_inModelspace, vec2 uvCoords, float scale) {

    // calculate Z-displacement:
    float terrainZ = calculateTerrainZ_simple(uvCoords, scale);
    float vectorLengthOld = length(position_inModelspace);
    float vectorLengthNew = vectorLengthOld + (terrainZ / vectorLengthOld);
    return vectorLengthNew * position_inModelspace;

}

vec3 calculateDisplacedVertex_InModelSpace_minimal(vec3 position_inModelspace, float terrainZ) {

    float vectorLengthOld = length(position_inModelspace);
    float vectorLengthNew = vectorLengthOld + (terrainZ / vectorLengthOld);
    return vectorLengthNew * position_inModelspace;

}


//================================
vec3 approximateTerrainNormal(vec3 position_inModelspace, float worldScale) {
    // We will use small rotations around axises perpendicular to the given positionvector.
    // For that, an approximation of the angle is calculated to get similar behaviour for planets of different radius
    float angle = SAMPLE_ANGLE__NORMAL_VECTOR / length(position_inModelspace);

    // Now get the rotation-axises and matrices:
    vec3 axisA = calculateOrthogonalVector(position_inModelspace);
    vec3 axisB = cross(position_inModelspace, axisA);

    mat3 rotateA1 = rotationMatrix3d(axisA, angle);
    mat3 rotateA2 = rotationMatrix3d(axisA, -angle);
    mat3 rotateB1 = rotationMatrix3d(axisB, angle);
    mat3 rotateB2 = rotationMatrix3d(axisB, -angle);

    //calculate the points to sample the noisefunction at:
    vec3 virtVertexA1 = rotateA1 * position_inModelspace;
    vec3 virtVertexA2 = rotateA2 * position_inModelspace;
    vec3 virtVertexB1 = rotateB1 * position_inModelspace;
    vec3 virtVertexB2 = rotateB2 * position_inModelspace;

    //calculate points with modified "Z"-value:
    virtVertexA1 = calculateDisplacedVertex_InModelSpace_complete(virtVertexA1, worldScale);
    virtVertexA2 = calculateDisplacedVertex_InModelSpace_complete(virtVertexA2, worldScale);
    virtVertexB1 = calculateDisplacedVertex_InModelSpace_complete(virtVertexB1, worldScale);
    virtVertexB2 = calculateDisplacedVertex_InModelSpace_complete(virtVertexB2, worldScale);

    //calculate new normal (may point into wrong direction):
    vec3 normal = cross(virtVertexA1 - virtVertexA2, virtVertexB1 - virtVertexB2);

    //check if normal points into the correct direction, invert otherwise:
    if (dot(normal, position_inModelspace) / (length(normal) * length(position_inModelspace)) < 1.) {
        normal = -normal;
    }

    return normal;
}
