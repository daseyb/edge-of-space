
#define M_PI 3.1415926535897932384626433832795

//================================
// Formulas from: https://en.wikipedia.org/wiki/UV_mapping
// (13.12.2015)
//================================
// calculate UV-coords for a point on a sphere:
float calculateU(vec3 position_inModelSpace) {
    return 0.5 + atan(-position_inModelSpace.z, -position_inModelSpace.x) / (2* M_PI);
}
float calculateV(vec3 position_inModelSpace) {
    return 0.5 + asin(position_inModelSpace.y) / M_PI;
}
