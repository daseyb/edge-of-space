#pragma import "setup/vertex.glsl"

#pragma import "CommonDeferredFrag.glsl"
#pragma import "Utils.glsl"


void initShader(){};

vec4 color() {
  return vec4(0, 0, 0, 1);
}

vec4 emissive() {
  if(uHasAlbedoMap) {
    return texture(uTexture, vTexCoord).rgba * uEmissiveColor;
  }
  return vec4(0);
}

vec3 normal() {
  return vNormal;
}

vec4 specularSmoothness() {
    return vec4(0, 0, 0, 0);
}
