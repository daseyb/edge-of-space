#pragma import "CommonDeferredVert.glsl"

vec3 normal() {
  return aNormal;
}

vec2 texCoord() {
  return aTexCoord;
}

vec3 position() {
  return aPosition;
}