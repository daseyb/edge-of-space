#include <engine/scene/OrbitalSimulationSystem.hpp>

#include <ACGL/ACGL.hh>
#include <ACGL/OpenGL/Creator/ShaderProgramCreator.hh>
#include <ACGL/OpenGL/Creator/VertexArrayObjectCreator.hh>
#include <ACGL/OpenGL/Creator/Texture2DCreator.hh>
#include <ACGL/OpenGL/Objects.hh>
#include <ACGL/Base/Settings.hh>
#include <ACGL/Math/Math.hh>
#include <ACGL/OpenGL/Data/TextureLoadStore.hh>
#include <ACGL/OpenGL/Managers.hh>

#include <engine/ui/UISystem.hpp>

#include <engine/scene/Drawable.hpp>
#include <engine/scene/Transform.hpp>
#include <math.h>
#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include <engine/utils/stb_image_write.h>

#include <thread>

// ----------------------------------------------------------------------------
// PRECOMPUTATIONS
// ----------------------------------------------------------------------------
const float Rg = 6360.0;
const float Rt = 6420.0;
const float RL = 6421.0;

const int TRANSMITTANCE_W = 256;
const int TRANSMITTANCE_H = 64;

const int SKY_W = 64;
const int SKY_H = 16;

const int RES_R = 32;
const int RES_MU = 128;
const int RES_MU_S = 32;
const int RES_NU = 8;

const int transmittanceUnit = 1;
const int inscatterUnit = 3;
const int deltaSRUnit = 5;
const int deltaSMUnit = 6;

unsigned int transmittanceTexture;//unit 1, T table
unsigned int inscatterTexture;//unit 3, S table
unsigned int deltaSRTexture;//unit 5, deltaS table (Rayleigh part)
unsigned int deltaSMTexture;//unit 6, deltaS table (Mie part)

unsigned int transmittanceProg;
unsigned int inscatter1Prog;
unsigned int copyInscatter1Prog;

unsigned int fbo;

bool OrbitalSimulationSystem::startup() {
  RESOLVE_DEPENDENCY(m_settings);
  RESOLVE_DEPENDENCY(m_events);
  RESOLVE_DEPENDENCY(m_scene);
  RESOLVE_DEPENDENCY(m_renderer);

  m_events->subscribe<SimulateEvent>([this](const SimulateEvent &e) { simulateOrbitals(e.dt, e.totalTime); });

  m_events->subscribe<"DrawUI"_sh>([this]() {
    ImGui::Begin("Atmosphere", 0, ImGuiWindowFlags_AlwaysAutoResize);
    ImGui::Image((void*)transmittanceTexture, glm::vec2{ TRANSMITTANCE_W, TRANSMITTANCE_H }, { 0, 1 }, { 1, 0 });
    ImGui::End();

  }, 1);

  // Make a default geometry (TODO)
  auto vaoSphere = VertexArrayObjectCreator("icosphere2.obj").create();
  auto texture = Texture2DFileManager::the()->get(Texture2DCreator("checkerboard.png"));
  //auto texture = Texture2DFileManager::the()->get(Texture2DCreator("clownfishBunny.png"));

  // Debug lines
  auto vaoCircle = VertexArrayObjectCreator("circle2.obj").create();
  m_terrainNoise = Texture2DFileManager::the()->get(Texture2DCreator("noise.png"));

  orbitTorus = { vaoCircle };
  orbitTorus.vao->setMode(GL_LINES);
  glLineWidth(6);

  minimapItem = Geometry{ VertexArrayObjectCreator("circle_filled.obj").create() };
  playerIconGeometry = Geometry{ VertexArrayObjectCreator("cone.obj").create() };

  defaultGeom = { vaoSphere };
  defaultGeom.vao->setMode(GL_PATCHES);

  atmosphereGeom = { VertexArrayObjectCreator("icosphere2.obj").create() };

  auto testShader = ShaderProgramFileManager::the()->get(ShaderProgramCreator("lines")
    .attributeLocations(vaoCircle->getAttributeLocations())
    .fragmentDataLocations(m_renderer->getGBufferLocations()));

  auto itemShader =
      ShaderProgramFileManager::the()->get(ShaderProgramCreator("console/Minimap")
        .attributeLocations(minimapItem.vao->getAttributeLocations())
        .fragmentDataLocations(m_renderer->getGBufferLocations()));

  auto playerIconShader =
    ShaderProgramFileManager::the()->get(ShaderProgramCreator("console/Minimap")
      .attributeLocations(playerIconGeometry.vao->getAttributeLocations())
      .fragmentDataLocations(m_renderer->getGBufferLocations()));


  auto planets = m_settings->getAvailablePlanets();

  for (auto planet : planets) {
    auto planetShader = ShaderProgramFileManager::the()->get(
        ShaderProgramCreator(planet.shaders + "/" + planet.name)
            .attributeLocations(
                m_renderer->m_transformFeedbackVAO->getAttributeLocations())
            .fragmentDataLocations(m_renderer->getGBufferLocations()));

    planetMaterials[planet.name] = {glm::vec4(1, 1, 1, 1),
                                    glm::vec4(0, 0, 0, 1),
                                    m_terrainNoise,
                                    nullptr,
                                    nullptr,
                                    nullptr,
                                    planetShader,
                                    1,
                                    RenderQueue::OPAQUE};
  }

  auto moonShader = ShaderProgramFileManager::the()->get(ShaderProgramCreator("planets/moon/moon")
    .attributeLocations(m_renderer->m_transformFeedbackVAO->getAttributeLocations())
    .fragmentDataLocations(m_renderer->getGBufferLocations()));

  auto atmosphereShader = ShaderProgramFileManager::the()->get(ShaderProgramCreator("Atmosphere")
    .attributeLocations(vaoSphere->getAttributeLocations())
    .fragmentDataLocations(m_renderer->getTransparentLocations()));

  auto waterShader = ShaderProgramFileManager::the()->get(ShaderProgramCreator("WaterSurface")
    .attributeLocations(m_renderer->m_transformFeedbackVAO->getAttributeLocations())
    .fragmentDataLocations(m_renderer->getGBufferLocations()));

  moonMat = {glm::vec4(1, 1, 1, 1),
               glm::vec4(0, 0, 0, 1),
               texture,
               nullptr,
               nullptr,
               nullptr,
               moonShader,
               1,
               RenderQueue::OPAQUE, 
               GL_BACK};

  trajectoryMat = {glm::vec4(1.0, 1.0, 1.0, 1.0),
                   glm::vec4(1.0f, 0.1f, 0, 1.5),
                   texture,
                   nullptr,
                   nullptr,
                   nullptr,
                   testShader,
                   0,
                   RenderQueue::OPAQUE,
                   GL_BACK };

  atmosphereMat = {glm::vec4(0, 0, 1, 1.0),
                   glm::vec4(0, 0, .2, 1),
                   texture,
                   nullptr,
                   nullptr,
                   nullptr,
                   atmosphereShader,
                   0,
                   RenderQueue::TRANSPARENT,
                   GL_FRONT};

  waterMat = {glm::vec4(0, 0.2, 0.8, 1),
              glm::vec4(0, 0, .2, 1),
              texture,
              nullptr,
              nullptr,
              nullptr,
              waterShader,
              0,
              RenderQueue::OPAQUE,
              GL_BACK};

  minimapItemMaterial = {glm::vec4{0.0, 0.0, 0.0, 1.0},
                         glm::vec4{1.0, 0.0, 0.0, 1.0},
                         texture,
                         nullptr,
                         nullptr,
                         nullptr,
                         itemShader,
                         false,
                         RenderQueue::OPAQUE,
                         GL_BACK};

  playerIconMaterial = {glm::vec4{0.0, 0.0, 0.1, 1.0},
                        glm::vec4{0.0, 0.0, 1.0, 1.0},
                        texture,
                        nullptr,
                        nullptr,
                        nullptr,
                        playerIconShader,
                        false,
                        RenderQueue::OPAQUE,
                        GL_BACK};

  return true;
}

glm::dvec3 OrbitalSimulationSystem::applyKepler(Planet::Handle planet, float dt) {

    // compute the mean anomaly

    double meanAnomaly = planet->M0 + planet->dailyMotion * (dt);
    meanAnomaly = meanAnomaly - 2 * PI * floor(meanAnomaly / (2 * PI)); // Normalize angle
    planet->M0 = meanAnomaly;

    //meanAnomaly = 0;

    // Compute the eccentric anomaly
    double eccentricAnomaly = meanAnomaly + planet->eccentricity * sin(meanAnomaly) * (1.0 + planet->eccentricity * cos(meanAnomaly));

    
    if (planet->eccentricity > 0.06) {
        int limit = 10;
        double E_0 = eccentricAnomaly;
        double E_1 = E_0;
        do {
            E_0 = E_1;
            E_1 = E_0 - (E_0 - planet->eccentricity * sin(E_0) - meanAnomaly) / (1 - planet->eccentricity * cos(E_0));
            limit--;
        } while (fabs(E_1 - E_0) > 0.000000001 && limit > 0);
        eccentricAnomaly = E_1;
    }
    

    double xv = planet->semimajor * (cos(eccentricAnomaly) - planet->eccentricity);
    double yv = planet->semimajor * (sqrt(1.0 - planet->eccentricity * planet->eccentricity) * sin(eccentricAnomaly));

    // Calculate the true anomaly
    double trueAnomaly = atan2(yv, xv);

    // Radial distance to sun
    double r = sqrt(xv*xv + yv*yv);

    glm::dvec3 position;

    position.x = r*(cos(planet->ascendingNode)*cos(planet->longitudePerihelion + trueAnomaly) -
        sin(planet->ascendingNode)*sin(planet->longitudePerihelion + trueAnomaly)*cos(planet->inclination));

    position.z = r*(sin(planet->ascendingNode)*cos(planet->longitudePerihelion + trueAnomaly) +
        cos(planet->ascendingNode)*sin(planet->longitudePerihelion + trueAnomaly)*cos(planet->inclination));

    position.y = r*(sin(planet->longitudePerihelion + trueAnomaly)*sin(planet->inclination));

    return position;
}


void OrbitalSimulationSystem::simulateOrbitals(float dt, float totalTime) {

  auto planets = m_scene->entities_with_components<Planet, Transform>();

  Planet::Handle planet;
  Transform::Handle transform;
  glm::dvec3 pos;

  for (auto p : planets) {
    p.unpack<Planet, Transform>(planet, transform);

    // dt here is interpreted as a DAY NUMBER. TODO: Conversion/scale
    pos = applyKepler(planet, dt );
    
    if (planet->name != "Moon") {
      minimapItems[p.id()].component<Transform>()->position = glm::dvec3(pos.x, pos.z, pos.y -70 );
    }

    pos = glm::dvec3(AUToWorld(pos.x), AUToWorld(pos.y), AUToWorld(pos.z));

    transform->position = glm::dvec3(pos.x, pos.y, pos.z);
  }
}

using namespace std;
string* loadFile(const string &fileName) {
  string* result = new string();
  ifstream file(fileName.c_str());
  if (!file) {
    std::cerr << "Cannot open file " << fileName << endl;
    throw exception();
  }
  string line;
  while (getline(file, line)) {
    *result += line;
    *result += '\n';
  }
  file.close();
  return result;
}

unsigned int loadProgram(const vector<string> &files) {
  unsigned int programId = glCreateProgram();
  unsigned int vertexShaderId = glCreateShader(GL_VERTEX_SHADER);
  unsigned int fragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);
  glAttachShader(programId, vertexShaderId);
  glAttachShader(programId, fragmentShaderId);

  int n = files.size();
  string **strs = new string*[n];
  const char** lines = new const char*[n + 2];
  bool geo = false;
  for (int i = 0; i < n; ++i) {
    string* s = loadFile("data/shader/atmosphere/" + files[i]);
    strs[i] = s;
    lines[i + 2] = s->c_str();
    if (strstr(lines[i + 2], "_GEOMETRY_") != NULL) {
      geo = true;
    }
  }

  lines[0] = "#version 130\n";
  lines[1] = "#define _VERTEX_\n";
  glShaderSource(vertexShaderId, n + 2, lines, NULL);
  glCompileShader(vertexShaderId);

  if (geo) {
    unsigned geometryShaderId = glCreateShader(GL_GEOMETRY_SHADER);
    glAttachShader(programId, geometryShaderId);
    lines[1] = "#define _GEOMETRY_\n";
    glShaderSource(geometryShaderId, n + 2, lines, NULL);
    glCompileShader(geometryShaderId);
    glProgramParameteri(programId, GL_GEOMETRY_INPUT_TYPE_EXT, GL_TRIANGLES);
    glProgramParameteri(programId, GL_GEOMETRY_OUTPUT_TYPE_EXT, GL_TRIANGLE_STRIP);
    glProgramParameteri(programId, GL_GEOMETRY_VERTICES_OUT_EXT, 3);
  }

  lines[1] = "#define _FRAGMENT_\n";
  glShaderSource(fragmentShaderId, n + 2, lines, NULL);
  glCompileShader(fragmentShaderId);

  glLinkProgram(programId);

  for (int i = 0; i < n; ++i) {
    delete strs[i];
  }
  delete[] strs;
  delete[] lines;

  return programId;
}

void drawQuad() {
  VertexArrayObject vao;
  vao.bind();
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}



void setLayer(unsigned int prog, int layer) {
  double r = layer / (RES_R - 1.0);
  r = r * r;
  r = sqrt(Rg * Rg + r * (Rt * Rt - Rg * Rg)) + (layer == 0 ? 0.01 : (layer == RES_R - 1 ? -0.001 : 0.0));
  double dmin = Rt - r;
  double dmax = sqrt(r * r - Rg * Rg) + sqrt(Rt * Rt - Rg * Rg);
  double dminp = r - Rg;
  double dmaxp = sqrt(r * r - Rg * Rg);
  glUniform1f(glGetUniformLocation(prog, "r"), float(r));
  glUniform4f(glGetUniformLocation(prog, "dhdH"), float(dmin), float(dmax), float(dminp), float(dmaxp));
  glUniform1i(glGetUniformLocation(prog, "layer"), layer);
}


void precompute() {
  glActiveTexture(GL_TEXTURE0 + transmittanceUnit);
  glGenTextures(1, &transmittanceTexture);
  glBindTexture(GL_TEXTURE_2D, transmittanceTexture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, TRANSMITTANCE_W, TRANSMITTANCE_H, 0, GL_RGB, GL_FLOAT, NULL);

  glActiveTexture(GL_TEXTURE0 + inscatterUnit);
  glGenTextures(1, &inscatterTexture);
  glBindTexture(GL_TEXTURE_3D, inscatterTexture);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
  glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
  glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA16F, RES_MU_S * RES_NU, RES_MU, RES_R, 0, GL_RGB, GL_FLOAT, NULL);

  glActiveTexture(GL_TEXTURE0 + deltaSRUnit);
  glGenTextures(1, &deltaSRTexture);
  glBindTexture(GL_TEXTURE_3D, deltaSRTexture);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
  glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
  glTexImage3D(GL_TEXTURE_3D, 0, GL_RGB16F, RES_MU_S * RES_NU, RES_MU, RES_R, 0, GL_RGB, GL_FLOAT, NULL);

  glActiveTexture(GL_TEXTURE0 + deltaSMUnit);
  glGenTextures(1, &deltaSMTexture);
  glBindTexture(GL_TEXTURE_3D, deltaSMTexture);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
  glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
  glTexImage3D(GL_TEXTURE_3D, 0, GL_RGB16F, RES_MU_S * RES_NU, RES_MU, RES_R, 0, GL_RGB, GL_FLOAT, NULL);

  vector<string> files;
  files.push_back("common.glsl");
  files.push_back("transmittance.glsl");
  transmittanceProg = loadProgram(files);

  files.clear();
  files.push_back("common.glsl");
  files.push_back("inscatter1.glsl");
  inscatter1Prog = loadProgram(files);

  files.clear();
  files.push_back("common.glsl");
  files.push_back("copyInscatter1.glsl");
  copyInscatter1Prog = loadProgram(files);

  glGenFramebuffers(1, &fbo);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  glReadBuffer(GL_COLOR_ATTACHMENT0);
  glDrawBuffer(GL_COLOR_ATTACHMENT0);

  // computes transmittance texture T (line 1 in algorithm 4.1)
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, transmittanceTexture, 0);
  glViewport(0, 0, TRANSMITTANCE_W, TRANSMITTANCE_H);
  glUseProgram(transmittanceProg);
  drawQuad();

  // computes single scattering texture deltaS (line 3 in algorithm 4.1)
  // Rayleigh and Mie separated in deltaSR + deltaSM
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, deltaSRTexture, 0);
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, deltaSMTexture, 0);
  unsigned int bufs[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
  glDrawBuffers(2, bufs);
  glViewport(0, 0, RES_MU_S * RES_NU, RES_MU);
  glUseProgram(inscatter1Prog);
  glUniform1i(glGetUniformLocation(inscatter1Prog, "transmittanceSampler"), transmittanceUnit);
  for (int layer = 0; layer < RES_R; ++layer) {
    setLayer(inscatter1Prog, layer);
    drawQuad();
  }
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, 0, 0);
  glDrawBuffer(GL_COLOR_ATTACHMENT0);

  // copies deltaS into inscatter texture S (line 5 in algorithm 4.1)
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, inscatterTexture, 0);
  glViewport(0, 0, RES_MU_S * RES_NU, RES_MU);
  glUseProgram(copyInscatter1Prog);
  glUniform1i(glGetUniformLocation(copyInscatter1Prog, "deltaSRSampler"), deltaSRUnit);
  glUniform1i(glGetUniformLocation(copyInscatter1Prog, "deltaSMSampler"), deltaSMUnit);
  for (int layer = 0; layer < RES_R; ++layer) {
    setLayer(copyInscatter1Prog, layer);
    drawQuad();
  }

  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  glFinish();
}

void precomputeAtmosphereLookupTextures(double molecularNumberDensity,
                                       double airIOR, glm::dvec3 rgbWavelengths,
                                       double planetRadius,
                                       double atmosphereRadius,
                                       double averageDensityHeight, double g,
                                       SharedTexture2D& transmittanceText, SharedTexture3D& inscatteringText) {

  precompute();
  transmittanceText = std::make_shared<Texture2D>(GL_RGBA, transmittanceTexture);
  transmittanceText->setMinFilter(GL_LINEAR);
  transmittanceText->setMagFilter(GL_LINEAR);


  inscatteringText = std::make_shared<Texture3D>(GL_RGBA, inscatterTexture);
  inscatteringText->setMinFilter(GL_LINEAR);
  inscatteringText->setMagFilter(GL_LINEAR);
}

// Mass is in solar masses!
Entity OrbitalSimulationSystem::addPlanet(Transform::Handle sun, std::string n, double m, double r, double e, double a, double i, double N, double P, double T, std::string planetType) {
    // Mass, Radius, Eccentricity, Semimajor axis, Inclination, Ascending Node, Arg. of Periapsis, time at perihelion   

  auto planetEntity = m_scene->create();
  auto planetTransform = planetEntity.assign<Transform>();
  planetTransform->parent = sun;  
  //planetEntity.assign<Light>(glm::vec4(1, 1, 1, 1), glm::vec3(1, 0, 0), false);

  // Mass, Radius, Eccentricity, Semimajor axis, Inclination, Ascending Node, Arg. of Periapsis, time at perihelion
  auto planetComponent = planetEntity.assign<Planet>(n, m, r, e, a, i, N, P, T);

  // Move the planet to the right position:
  glm::dvec3 posInAU = applyKepler(planetComponent, 180);

  glm::dvec3 pos = glm::dvec3(AUToWorld(posInAU.x), AUToWorld(posInAU.y), AUToWorld(posInAU.z));


  planetTransform->scale =  glm::dvec3(solarToWorld(r), solarToWorld(r), solarToWorld(r));
  planetTransform->position = pos;

  auto planetData = m_settings->getPlanet(planetType);

  planetEntity.assign<Drawable>(defaultGeom, planetMaterials[planetType], 2, m_renderer->getRenderPassId("Main"_sh));

  // Add an atmosphere to the planet
  
  double Rg = planetTransform->scale.x;
  double Rt = planetTransform->scale.x * 1.05;
  static SharedTexture2D transmittance;
  static SharedTexture3D inscattering;

  if (transmittance == nullptr) {
    precomputeAtmosphereLookupTextures(2.55 * glm::pow(10.0, -11.0), // premultiply by nm^4 => 25 - 4*9
      1.000206102, glm::dvec3{ 680, 550, 440 }, // in nm
      6360000, 6420000, 8000, 0.76, transmittance, inscattering);
  }

  if (planetData.hasAtmosphere) {
    atmosphereMat.mainTexture = transmittance;
    atmosphereMat.specularSmoothnessTexture = inscattering;
    atmosphereMat.emissiveColor = { Rg, Rt, 0, 0 };
  
    auto atmosphere = m_scene->create();
    auto atmosphereTransform = atmosphere.assign<Transform>();
    atmosphereTransform->parent = planetTransform;

    atmosphere.assign<Drawable>(atmosphereGeom, atmosphereMat, 0, m_renderer->getRenderPassId("Main"_sh));
    atmosphereTransform->scale = glm::dvec3(Rt);
  }


  if (planetData.hasWater) {
      // Add simple water as well
      auto water = m_scene->create();
      auto waterTransform = water.assign<Transform>();
      waterTransform->parent = planetTransform;
      water.assign<Drawable>(defaultGeom, waterMat, 1, 1);
      waterTransform->scale = planetTransform->scale * 1.0;
  }

  if (planetData.moonCount != 0) {
      // Let's add a moon!
      auto moon = m_scene->create();
      auto moonTransform = moon.assign<Transform>();
      moonTransform->parent = planetTransform;
      moon.assign<Drawable>(defaultGeom, moonMat, 0, m_renderer->getRenderPassId("Main"_sh));
      double moonRad = 0.002495833333333;
      auto moonComponent = moon.assign<Planet>("Moon", 0.000000036939686, moonRad, 0.205633, 0.08, 0.0898041713, 5.4583095414, 1.671072474, 0 );

      // change starting point
      //float t = static_cast <float> (rand()) / static_cast <float> (RAND_MAX / 10.0f);
      glm::dvec3 moonPos = applyKepler(moonComponent, 0);
      moonPos = glm::dvec3(AUToWorld(moonPos.x), AUToWorld(moonPos.y), AUToWorld(moonPos.z));

      moonTransform->position = moonPos;
      moonTransform->scale = glm::dvec3(solarToWorld(moonRad), solarToWorld(moonRad), solarToWorld(moonRad));
  }

  // Add the transformation matrixes to calculate the trajectory from a standard circle:
  double b = a*sqrt(1 - e*e);
  double distLength = (sqrt(a*a - b*b));

  glm::dvec3 dist = posInAU * (distLength / glm::length(posInAU));
  glm::dvec3 tScale = glm::dvec3(b, 1, a);

  glm::dmat4 tRot = glm::rotate<double>(P, glm::dvec3( 0, -1, 0));
  tRot = tRot * glm::rotate<double>(i, glm::dvec3(-1, 0, 0));
    
  auto traj = m_scene->create();
  auto trajTransform = traj.assign<Transform>();
  auto drawable = traj.assign<Drawable>(orbitTorus, trajectoryMat, 0, m_renderer->getRenderPassId("Minimap"_sh));
  trajTransform->rotation = glm::rotate<double>(M_PI / 2, glm::dvec3(1, 0, 0)) * tRot;
  trajTransform->scale = tScale;
  trajTransform->position = glm::dvec3(0 - dist.x, 0 - dist.z, -70);

  trajectories.push_back(traj);

    
  auto mmItem = m_scene->create();
  mmItem.assign<Drawable>(minimapItem, minimapItemMaterial, 0, m_renderer->getRenderPassId("Minimap"_sh));
  
  auto itemTransform = mmItem.assign<Transform>();
  itemTransform->position = { posInAU.x, posInAU.z, posInAU.y - 70 };
  itemTransform->scale = { 2.0, 2.0, 2.0 };
  itemTransform->rotation = glm::rotate<double>(M_PI/2, glm::dvec3(1, 0, 0));  

  minimapItems[planetEntity.id()] = mmItem;
    
  return planetEntity;
}


void OrbitalSimulationSystem::showTrajectories() {
    for (Entity e : trajectories) {
        //e.component<Drawable>()->visible = !e.component<Drawable>()->visible;
    }
}

// Right now planets are 10 times closer to each other than in reality and 10 times bigger

double OrbitalSimulationSystem::AUToWorld(double au) {
  return distanceScale * solarToWorld(AUToSolar(au));
}

double OrbitalSimulationSystem::solarToWorld(double solar) {
  return solar * solarRadius * scaleFactor;
}

double OrbitalSimulationSystem::solarToAU(double s) {
  return s * 0.004652472637379 / distanceScale;
}

double OrbitalSimulationSystem::AUToSolar(double au) {
  return au * 214.9394693836;
}

double OrbitalSimulationSystem::worldToSolar(double w) {
  return w / solarRadius / scaleFactor;
}

double OrbitalSimulationSystem::worldToAU(double w) {
  return solarToAU(worldToSolar(w));
}

glm::dvec3 OrbitalSimulationSystem::worldToAU(glm::dvec3 w) {
  return glm::dvec3(solarToAU(worldToSolar(w.x)), solarToAU(worldToSolar(w.y)), solarToAU(worldToSolar(w.z)));
}

void OrbitalSimulationSystem::shutdown() {}

