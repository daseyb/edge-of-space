#include <engine/scene/scenes/AtmosphereTestScene.hpp>

#include <engine/ui/UISystem.hpp>

#include <ACGL/ACGL.hh>

#include <ACGL/OpenGL/Objects.hh>
#include <ACGL/Base/Settings.hh>
#include <ACGL/Math/Math.hh>
#include <ACGL/OpenGL/Data/TextureLoadStore.hh>
#include <ACGL/OpenGL/Managers.hh>

#include <ACGL/OpenGL/Creator/ShaderProgramCreator.hh>
#include <ACGL/OpenGL/Creator/VertexArrayObjectCreator.hh>
#include <ACGL/OpenGL/Creator/Texture2DCreator.hh>

#include <engine/scene/Transform.hpp>
#include <engine/scene/Drawable.hpp>
#include <engine/scene/Planet.hpp>

#include <engine/events/MouseEvent.hpp>
#include <engine/events/KeyboardEvent.hpp>

#include <engine/graphics/BloomPostFX.hpp>


using namespace ACGL::OpenGL;
using namespace ACGL::Base;
using namespace ACGL::Utils;

void AtmosphereTestScene::switchConsole() {
  if (isInterpolatingCamera) return;
  m_player->setHasControl(false);

  if (isFocusedOnConsole) {
    camStartPos = m_player->getCockpitPosition();
    camTargetPos = { 0, 0 , 0 };
    camStartRot = glm::quat_cast(m_player->getRotation());
    camTargetRot = glm::quat();
    m_console.pause();
    m_renderer->setRenderPassActive("Main"_sh, true);
    m_renderer->setRenderPassActive("Skybox"_sh, true);
    m_renderer->setRenderPassActive("Minimap"_sh, true);
    cockpitSoundSource->setVolume(0);
    cockpitSoundSource->play(-1);
    m_renderer->setRenderPassActive("Console"_sh, false);

  } else {
    // TODO: Fix returning to console!
    return;
    camStartPos = m_player->getCockpitPosition();
    camTargetPos = CAM_CONSOLE_POS;
    camStartRot = glm::quat_cast(m_player->getRotation());
    camTargetRot = glm::quat_cast(CAM_CONSOLE_ROT);
    m_console.play();
  }

  isFocusedOnConsole = !isFocusedOnConsole;
  interpolationTime = 0;
  totalInterpolationTime = 4;
  isInterpolatingCamera = true;
}

bool AtmosphereTestScene::startup() {
  if (!Scene::startup()) {
    return false;
  }

  RESOLVE_DEPENDENCY(m_player);
  RESOLVE_DEPENDENCY(m_orbitals);

  m_renderer->addEffect<BloomPostFX>();

  consoleCamera = m_sceneGraph->create();
  consoleCamera.assign<Transform>();
  consoleCamera.assign<Camera>(50, 0.1, 50);

  m_audio->setListener(consoleCamera);

  m_renderer->setRenderPassActive("Main"_sh, false);
  m_renderer->setRenderPassActive("Skybox"_sh, false);
  m_renderer->setRenderPassActive("Minimap"_sh, false);

  m_renderer->setRenderPassSSAO("Cockpit"_sh, m_settings->getQualitySetting() == QualitySetting::High && m_settings->ssaoEnabled());

  m_renderer->addRenderPass(consoleCamera, "Console"_sh, ScreenSpaceSize::HALF);

  planetViewCam = m_sceneGraph->create();
  planetViewCam.assign<Transform>();
  planetViewCam.assign<Camera>(50, 0.1, 50);
  m_renderer->addRenderPass(planetViewCam, "PlanetView"_sh, ScreenSpaceSize::HALF);
  m_renderer->setRenderPassOnlyTexture("PlanetView"_sh, true);

  m_renderer->addRenderPass(planetViewCam, "LogoView"_sh, ScreenSpaceSize::HALF);
  m_renderer->setRenderPassOnlyTexture("LogoView"_sh, true);

  m_console.m_events = m_events;
  m_console.m_renderer = m_renderer;
  m_console.m_sceneGraph = m_sceneGraph;
  m_console.m_audio = m_audio;

  auto textTexture =
    Texture2DFileManager::the()->get(Texture2DCreator("none.png"));
  auto textGeom = Geometry{ VertexArrayObjectCreator("loading_text.obj").create() };

  auto textShader =
    ShaderProgramFileManager::the()->get(ShaderProgramCreator("console/Title")
      .attributeLocations(textGeom.vao->getAttributeLocations())
      .fragmentDataLocations(m_renderer->getGBufferLocations()));

  loadingText = m_sceneGraph->create();
  loadingText.assign<Drawable>(textGeom,
    Material{ glm::vec4{ 1, 1, 1, 1 }, glm::vec4{ 1, 1, 1, 1 } * 10,
    textTexture, nullptr, nullptr, nullptr, textShader,
    false, RenderQueue::OPAQUE, GL_BACK },
    0, m_renderer->getRenderPassId("Console"_sh));

  auto textTransform = loadingText.assign<Transform>();
  textTransform->position = { 0, -3, -11 };
  loadingText.assign<Light>(glm::vec4(0, 0.2, 1.0, 5), glm::vec3(1, 0, 0),
    false, LightType::POINT, m_renderer->getRenderPassId("Console"_sh));


  SDL_SetRelativeMouseMode(SDL_TRUE);
  m_player->setHasControl(false);
  m_player->setCockpitPosition(CAM_CONSOLE_POS);
  m_player->setRotation(CAM_CONSOLE_ROT);

  m_events->subscribe<SimulateEvent>([this](const SimulateEvent &e) {

    if (didOneFrame && !resourceLoadingFinished) {
      loadMainSceneResources();
      switchToMainScene();
      m_renderer->setRenderPassOnlyTexture("Console"_sh, true);
      m_console.startup();      
    }

    didOneFrame = true;

    if (!resourceLoadingFinished) {
      return;
    }

    sunLight->dir = -glm::normalize(m_player->getWorldPosition());
    cockpitSunLight->dir =
      glm::vec3(glm::dvec4(sunLight->dir, 0) * m_player->getShipRotation());

    if (isFocusedOnConsole) {
      if (m_console.update(e, m_player->getKeyTable())) {
        switchConsole();
      }
    }

    if (isInterpolatingCamera) {
      double interpolationPercent = 1.0 - glm::pow(1 - interpolationTime / totalInterpolationTime, 2);
      m_player->setCockpitPosition(camStartPos + (camTargetPos-camStartPos)*interpolationPercent);

      interpolationPercent = 1.0 - glm::pow(1 - interpolationTime / totalInterpolationTime, 5);

      consoleSoundSource->setVolume((1.0f - interpolationTime / totalInterpolationTime) * 0.5);
      cockpitSoundSource->setVolume(interpolationTime / totalInterpolationTime);

      glm::quat rot = glm::slerp(camStartRot, camTargetRot, (float)interpolationPercent);
      m_player->setRotation(glm::mat4_cast(rot));
      interpolationTime += e.dt;
      if (interpolationTime >= totalInterpolationTime) {
        m_player->setCockpitPosition(camTargetPos);
        m_player->setRotation(glm::mat4_cast(camTargetRot));
        isInterpolatingCamera = false;
        m_player->setHasControl(true);
        consoleSoundSource->setVolume(0);
        cockpitSoundSource->setVolume(1);
      }
    }

    double rotationSpeedFactor = (1.0 - exp(-glm::abs(m_player->getVelocity()) * 0.001)) * 3  * glm::sign(m_player->getVelocity());

    planetRotationSpeed += rotationSpeedFactor * e.dt;
    planetRotationSpeed = glm::sign(planetRotationSpeed) * glm::min(glm::abs(planetRotationSpeed), 3.0);
    planetRotationSpeed -= planetRotationSpeed * e.dt * 0.3;

    planetViewTransform->rotation = glm::rotate<double>(planetViewTransform->rotation, planetRotationSpeed * e.dt, glm::dvec3{ 0.0, 1.0, 0.0 });

    logoViewTransform->rotation = glm::inverse(m_player->getShipRotation()); // glm::rotate<double>(logoViewTransform->rotation, 1.0 * e.dt, glm::dvec3{ 0.0, 1.0, 0.0 });

  });

  m_events->subscribe<KeyboardEvent>([this](const KeyboardEvent &e) {
    switch (e.originalEvent.key.keysym.scancode) {
    case SDL_SCANCODE_TAB:
        if (e.originalEvent.key.type == SDL_KEYDOWN) {
          m_player->attachToParent(earth);
          float r = earth.component<Transform>()->scale.x;
          m_player->setPosition(glm::vec3(0, 0, r * 3.5));
          m_player->setRotation(glm::mat4());
        }
      break;
    case SDL_SCANCODE_RETURN:
      if (e.originalEvent.key.type == SDL_KEYDOWN) {
        switchConsole();
      }
      break;
    case SDL_SCANCODE_R:
      if (e.originalEvent.key.type == SDL_KEYDOWN) {
        ShaderProgramFileManager::the()->updateAll();
        Texture2DFileManager::the()->updateAll();
      }
    default:
      break;
    }
  });

  // Subscribe to the UI drawing event.
  // This is called once per frame
  m_events->subscribe<"DrawUI"_sh>([this]() {

    ImGui::Begin("Camera Control", 0,
                 ImGuiWindowFlags_::ImGuiWindowFlags_AlwaysAutoResize);

    float r;
    if (ImGui::Button("Sun/Global", glm::vec2(100, 20))) {
      // Reset the camera to sun coords
      m_player->attachToParent(sun);
      r = sun.component<Transform>()->scale.x;
      m_player->setPosition(glm::dvec3(0, 0, r * 1.2));
      m_player->setRotation(glm::dmat4());
    }

    if (ImGui::Button("Earth", glm::vec2(100, 20))) {
      m_player->attachToParent(earth);
      r = earth.component<Transform>()->scale.x;
      m_player->setPosition(glm::dvec3(0, 0, r * 1.2));
      m_player->setRotation(glm::dmat4());
    }

    ImGui::End();
  });

  return true;
}

void AtmosphereTestScene::switchToMainScene() {


  {
    auto skyboxPassId = m_renderer->getRenderPassId("Skybox"_sh);

    skybox = m_sceneGraph->create();
    skybox.assign<Transform>();
    skybox.assign<Drawable>(skyboxGeometry, skyboxMaterial, 0, skyboxPassId);
    skybox.assign<Light>(glm::vec4(1, 1, 1, 1), glm::vec3(1, 0, 0), false,
                         LightType::POINT, skyboxPassId);
  }

  {
    auto mainPassId = m_renderer->getRenderPassId("Main"_sh);

    // Let's create a placeholder sun
    sun = m_sceneGraph->create();
    // Add a transform component to it so we are able to position it in space
    auto sunTransform = sun.assign<Transform>();
    // Add a Drawable component to it so the renderer has something to draw
    sun.assign<Drawable>(sunGeometry, sunMaterial, 0, m_renderer->getRenderPassId("Main"_sh));
    float scale = m_orbitals->solarRadius * m_orbitals->scaleFactor * 0.1;
    sunTransform->scale = glm::dvec3(scale);
    sunLight = sun.assign<Light>(glm::vec4(1, 1, 1, 1.5), glm::vec3(1, 0, 0),
                                 false, LightType::DIRECTIONAL, m_renderer->getRenderPassId("Main"_sh));

    // Let's create a placeholder sun
    sun2 = m_sceneGraph->create();
    // Add a transform component to it so we are able to position it in space
    auto sunTransform2 = sun2.assign<Transform>();
    sunMaterial.emissiveColor.a = 3.0;
    // Add a Drawable component to it so the renderer has something to draw
    sun2.assign<Drawable>(sunGeometry, sunMaterial, 0, m_renderer->getRenderPassId("Minimap"_sh));
    //float scale2 = m_orbitals->solarRadius * m_orbitals->scaleFactor * 0.1;
    sunTransform2->scale = glm::dvec3(0.4, 0.4, 0.4);
    sunTransform2->position = glm::dvec3(0, 0, -70);
    sunLight2 = sun2.assign<Light>(glm::vec4(1, 1, 1, 1), glm::vec3(1, 0, 0),
      false, LightType::POINT, m_renderer->getRenderPassId("Minimap"_sh));


    // create a planet!
    // Mass in solar masses, Radius in solar radii, Eccentricity, Semimajor
    // axis, Inclination, Ascending Node, Arg. of Periapsis, time at perihelion
    //																mass              radius
    //e
    // a        i          N          w

    mercury = m_orbitals->addPlanet(sunTransform, "Mercury", 0.000000165956, 0.003505316091954, 0.205633, 0.387098, 0.1222500, 0.84153, 0.50768, 0, "earth");
    venus = m_orbitals->addPlanet(sunTransform, "Venus", 0.00000243522, 0.008695402298851, 0.006778, 0.723330, 0.0592500, 1.33679, 0.95717, 0, "mars");
    earth = m_orbitals->addPlanet(sunTransform, "Earth", 0.000002988, 0.009153735632184, 0.016713, 1.000000, 0.0000000, 0.00000, 4.93533, 0, m_settings->getDefaultPlanetType());
    mars = m_orbitals->addPlanet(sunTransform, "Mars", 0.000000319716, 0.004870689655172, 0.093396, 1.523688, 0.0322851, 0.02333, 4.99858, 0, "mars");
    jupiter = m_orbitals->addPlanet(sunTransform, "Jupiter", 0.000954265748, 0.1004468390805, 0.048482, 5.202560, 0.0227500, 1.75150, 4.77905, 0, "jupiter");
    saturn = m_orbitals->addPlanet(sunTransform, "Saturn", 0.00028386, 0.08366666666667, 0.055580, 9.554750, 0.0434412, 1.98230, 5.92169, 0, "jupiter");
    uranus = m_orbitals->addPlanet(sunTransform, "Uranus", 0.00004344552, 0.03643965517241, 0.047292, 19.18176, 0.0134948, 1.29060, 1.68516, 0, "jupiter");
    neptune = m_orbitals->addPlanet(sunTransform, "Neptune", 0.0000512442, 0.03537643678161, 0.008598, 30.05814, 0.0002355, 2.29810, 4.76243, 0, "jupiter");


    m_player->attachToParent(earth);
    double r = earth.component<Transform>()->scale.x;
    m_player->setPosition(glm::dvec3(-r * 0.5, 0, r * 2.5));
  }

  {
    auto cockpitPassId = m_renderer->getRenderPassId("Cockpit"_sh);

    cockpit = m_sceneGraph->create();
    auto cockpitDrawable = cockpit.assign<Drawable>(
        cockpitGeometry, cockpitMaterial, 0, cockpitPassId);
    auto cockpitTransform = cockpit.assign<Transform>();
    cockpitTransform->position = glm::dvec3(0, 0, 0);
    cockpitDrawable->visible = true;
    auto cockpitLight =
        cockpit.assign<Light>(glm::vec4(1, 1, 1, 0.0), glm::vec3(1, 0, 0),
                              false, LightType::POINT, cockpitPassId);


    cockpitSoundSource = cockpit.assign<SoundSource>(soundTrackIntro);

    cockpitSun = m_sceneGraph->create();
    cockpitSun.assign<Transform>();
    cockpitSunLight =
        cockpitSun.assign<Light>(glm::vec4(1, 1, 1, 3), glm::vec3(1, 0, 0),
                                 true, LightType::DIRECTIONAL, cockpitPassId);


    auto lightLeft = m_sceneGraph->create();
    lightLeft.assign<Transform>()->position = { -0.6, 0.6, -2.34 };
    lightLeft.assign<Light>(glm::vec4(1.0, 0.5, 0.3, 0.5), glm::vec3(1, 0, 0), false, LightType::POINT, cockpitPassId);

    auto lightRight= m_sceneGraph->create();
    lightRight.assign<Transform>()->position = { 0.6, 0.6, -2.34 };
    lightRight.assign<Light>(glm::vec4(0.3, 1.0, 0.4, 0.5), glm::vec3(1, 0, 0), false, LightType::POINT, cockpitPassId);

    auto console = m_sceneGraph->create();
    console.assign<Drawable>(
        consoleGeom,
        Material{glm::vec4{1, 1, 1, 1}, glm::vec4{1, 1, 1, 1},
                 m_renderer->getRenderPassTarget("Console"_sh), nullptr, nullptr, nullptr,
                 skyboxMaterial.prog, false, RenderQueue::OPAQUE, GL_NONE},
        0, cockpitPassId);
    auto consoleTransform = console.assign<Transform>();

    consoleSoundSource = console.assign<SoundSource>(soundTrackMain);
    consoleSoundSource->setVolume(0.5);
    consoleSoundSource->play(-1);

    auto consoleFrontLeft = m_sceneGraph->create();
    consoleFrontLeft.assign<Drawable>(
      consoleFrontLeftGeom,
      Material{ glm::vec4{ 0.2, 0.2, 0.2, 1 }, glm::vec4{ 1, 1, 1, 1 },
      nullptr, nullptr, nullptr, m_renderer->getRenderPassTarget("LogoView"_sh),
      defaultPbrProg, true, RenderQueue::OPAQUE, GL_NONE },
      0, cockpitPassId);
    consoleFrontLeft.assign<Transform>();

    auto consoleFrontRight = m_sceneGraph->create();
    consoleFrontRight.assign<Drawable>(
      consoleFrontRightGeom,
      Material{ glm::vec4{ 1, 1, 1, 1 }, glm::vec4{ 1, 1, 1, 1 },
      m_renderer->getRenderPassTarget("PlanetView"_sh), nullptr, nullptr, nullptr,
      skyboxMaterial.prog, true, RenderQueue::OPAQUE, GL_NONE },
      0, cockpitPassId);
    consoleFrontRight.assign<Transform>();

    auto consoleBackLeft = m_sceneGraph->create();
    consoleBackLeft.assign<Drawable>(
      consoleBackLeftGeom,
      Material{ glm::vec4{ 1, 1, 1, 1 }, glm::vec4{ 1, 1, 1, 1 },
      m_renderer->getRenderPassTarget("Minimap"_sh), nullptr, nullptr, nullptr,
      skyboxMaterial.prog, true, RenderQueue::OPAQUE, GL_NONE },
      0, cockpitPassId);
    consoleBackLeft.assign<Transform>();

    auto consoleBackRight = m_sceneGraph->create();
    consoleBackRight.assign<Drawable>(
      consoleBackRightGeom,
      Material{ glm::vec4{ 1, 1, 1, 1 }, glm::vec4{ 1, 1, 1, 1 },
      outOfOrderTexture, nullptr, nullptr, nullptr,
      skyboxMaterial.prog, true, RenderQueue::OPAQUE, GL_NONE },
      0, cockpitPassId);
    consoleBackRight.assign<Transform>();


    auto consoleMiddleLeft= m_sceneGraph->create();
    consoleMiddleLeft.assign<Drawable>(
      consoleMiddleLeftGeom,
      Material{ glm::vec4{ 1, 1, 1, 1 }, glm::vec4{ 1, 1, 1, 1 },
      outOfOrderTexture, nullptr, nullptr, nullptr,
      skyboxMaterial.prog, true, RenderQueue::OPAQUE, GL_NONE },
      0, cockpitPassId);
    consoleMiddleLeft.assign<Transform>()->scale = { -1, 1, 1 };

    auto consoleMiddleRight = m_sceneGraph->create();
    consoleMiddleRight.assign<Drawable>(
      consoleMiddleRightGeom,
      Material{ glm::vec4{ 1, 1, 1, 1 }, glm::vec4{ 1, 1, 1, 1 },
      outOfOrderTexture, nullptr, nullptr, nullptr,
      skyboxMaterial.prog, true, RenderQueue::OPAQUE, GL_NONE },
      0, cockpitPassId);
    consoleMiddleRight.assign<Transform>()->scale = { -1, 1, 1 };

    auto windShield = m_sceneGraph->create();
    windShield.assign<Drawable>(
      windShieldGeom,
      windshieldMaterial,
      0, cockpitPassId);
    windShield.assign<Transform>();
  }

  {
    auto planetViewPassId = m_renderer->getRenderPassId("PlanetView"_sh);
    auto planetView = m_sceneGraph->create();
    planetView.assign<Drawable>(
      planetViewGeom,
      Material{ glm::vec4{ 1, 1, 1, 1 }, glm::vec4{ 0.3, 1.0, 0.4, 1 },
      nullptr, nullptr, nullptr, nullptr,
      defaultPbrProg, true, RenderQueue::OPAQUE, GL_NONE },
      0, planetViewPassId);
    planetViewTransform = planetView.assign<Transform>();
    planetViewTransform->position = { 0.0, 0.0, -5.0 };;
    planetViewTransform->scale = { 1.5, 1.5, 1.5 };

    planetView.assign<Light>(glm::vec4(1, 1, 1, 0.2), glm::vec3(1, 0, 0),
      false, LightType::POINT, planetViewPassId);
  }

  {
    auto logoViewPassId = m_renderer->getRenderPassId("LogoView"_sh);
    auto logoView = m_sceneGraph->create();
    logoView.assign<Drawable>(
      logoViewGeom,
      Material{ glm::vec4{ 1, 1, 1, 1 }, glm::vec4{ 1.0, 0.5, 0.3, 1 },
      nullptr, nullptr, nullptr, nullptr,
      defaultPbrProg, true, RenderQueue::OPAQUE, GL_NONE },
      0, logoViewPassId);
    logoViewTransform = logoView.assign<Transform>();
    logoViewTransform->position = { 0.0, 0.0, -5.0 };;
    logoViewTransform->scale = { 0.75, 0.75, 0.75 };

    logoView.assign<Light>(glm::vec4(1, 1, 1, 0.2), glm::vec3(1, 0, 0),
      false, LightType::POINT, logoViewPassId);
  }

  loadingText.component<Drawable>()->visible = false;
  loadingText.destroy();
}

void AtmosphereTestScene::loadMainSceneResources() {


  auto qualitySetting = m_settings->getQualitySetting();
  {
    auto vaoSkybox = VertexArrayObjectCreator("skybox.obj").create();
    auto skyboxShader = ShaderProgramFileManager::the()->get(
        ShaderProgramCreator("Skybox")
            .attributeLocations(vaoSkybox->getAttributeLocations())
            .fragmentDataLocations(m_renderer->getGBufferLocations()));

    defaultPbrProg = ShaderProgramFileManager::the()->get(
      ShaderProgramCreator("PBR")
      .attributeLocations(vaoSkybox->getAttributeLocations())
      .fragmentDataLocations(m_renderer->getGBufferLocations()));


    auto skyboxTexName = qualitySetting == QualitySetting::High ? "skybox_large.png" : qualitySetting == QualitySetting::Medium ? "skybox_medium.png" : "skybox.png";

    auto skyboxTexture = Texture2DFileManager::the()->get(Texture2DCreator(skyboxTexName));

    skyboxGeometry = {vaoSkybox};
    skyboxMaterial = {glm::vec4{1, 1, 1, 1},
                      glm::vec4{0.05, 0.05, 0.05, 1},
                      skyboxTexture,
                      nullptr,
                      nullptr,
                      nullptr,
                      skyboxShader,
                      false,
                      RenderQueue::OPAQUE,
                      GL_BACK};
  }

  {
    auto vaoSun = VertexArrayObjectCreator("uvsphere.obj").create();
    auto checkboardTexture =
        Texture2DFileManager::the()->get(Texture2DCreator("checkerboard.png"));

    auto sunShader = ShaderProgramFileManager::the()->get(
      ShaderProgramCreator("PBR")
      .attributeLocations(vaoSun->getAttributeLocations())
      .fragmentDataLocations(m_renderer->getGBufferLocations()));

    sunGeometry = {vaoSun};

    sunMaterial = {glm::vec4{1, 1, 1, 1},
                   glm::vec4{2, 2, 1, 20},
                   checkboardTexture,
                   nullptr,
                   nullptr,
                   nullptr,
                   sunShader,
                   false,
                   RenderQueue::OPAQUE,
                   GL_BACK};
  }

  {
    // create cockpit
    auto vaoCockpit = VertexArrayObjectCreator("cockpit/cockpit_v2_outer.obj").create();
    auto cockpitShader = ShaderProgramFileManager::the()->get(
      ShaderProgramCreator("PBR")
      .attributeLocations(vaoCockpit->getAttributeLocations())
      .fragmentDataLocations(m_renderer->getGBufferLocations()));

    cockpitMaterial = {
        glm::vec4{0.4, 0.4, 0.4, 1}, glm::vec4{0, 0, 0, 1},
        Texture2DFileManager::the()->get(Texture2DCreator(
            "cockpit_v2/cockpit_v2_outer_DefaultMaterial_AlbedoTransparency.png")),
        Texture2DFileManager::the()->get(
            Texture2DCreator("cockpit_v2/cockpit_v2_outer_DefaultMaterial_Normal.png")),
        Texture2DFileManager::the()->get(Texture2DCreator(
            "cockpit_v2/cockpit_v2_outer_DefaultMaterial_SpecularSmoothness.png")),
        Texture2DFileManager::the()->get(
            Texture2DCreator("cockpit_v2/cockpit_v2_outer_DefaultMaterial_Emission.png")),
        cockpitShader, true, RenderQueue::OPAQUE, GL_NONE };

    cockpitGeometry = {vaoCockpit};

    windshieldMaterial = Material{ glm::vec4{ 0.2, 0.3, 1.0, 0.2 }, glm::vec4{ 0.02, 0.03, 0.05, 1 },
      Texture2DFileManager::the()->get(Texture2DCreator(
        "cockpit_v2/"
        "windshield_DefaultMaterial_AlbedoTransparency.png")),
      nullptr, nullptr, nullptr, cockpitShader, false,
      RenderQueue::OPAQUE, GL_NONE };

    consoleGeom = Geometry{ VertexArrayObjectCreator("cockpit/cockpit_v2_maindisplay.obj").create() };
    consoleFrontLeftGeom = Geometry{ VertexArrayObjectCreator("cockpit/cockpit_v2_console_front_left.obj").create() };
    consoleFrontRightGeom = Geometry{ VertexArrayObjectCreator("cockpit/cockpit_v2_console_front_right.obj").create() };
    consoleBackLeftGeom = Geometry{ VertexArrayObjectCreator("cockpit/cockpit_v2_console_back_left.obj").create() };
    consoleBackRightGeom = Geometry{ VertexArrayObjectCreator("cockpit/cockpit_v2_console_back_right.obj").create() };
    consoleMiddleLeftGeom = Geometry{ VertexArrayObjectCreator("cockpit/cockpit_v2_console_middle_left.obj").create() };
    consoleMiddleRightGeom = Geometry{ VertexArrayObjectCreator("cockpit/cockpit_v2_console_middle_right.obj").create() };
    windShieldGeom = Geometry{ VertexArrayObjectCreator("cockpit/cockpit_v2_windshield.obj").create() };


    outOfOrderTexture = Texture2DFileManager::the()->get(Texture2DCreator("cockpit_v2/outoforder.png"));

    soundTrackIntro = m_audio->createSound("GameAmbient.mp3", SoundMode::MODE_2D);
    soundTrackMain = m_audio->createSound("SpaceTheme.mp3", SoundMode::MODE_2D);

  }

  {
    planetViewGeom = Geometry{ VertexArrayObjectCreator("planet_wireframe.obj").create() };
    logoViewGeom = Geometry{ VertexArrayObjectCreator("title_text.obj").create() };
  }

  resourceLoadingFinished = true;
}

void AtmosphereTestScene::shutdown() {}
