#include <engine/scene/scenes/Console.hpp>

#include <ACGL/ACGL.hh>

#include <ACGL/OpenGL/Objects.hh>
#include <ACGL/Base/Settings.hh>
#include <ACGL/Math/Math.hh>
#include <ACGL/OpenGL/Data/TextureLoadStore.hh>
#include <ACGL/OpenGL/Managers.hh>

#include <ACGL/OpenGL/Creator/ShaderProgramCreator.hh>
#include <ACGL/OpenGL/Creator/VertexArrayObjectCreator.hh>
#include <ACGL/OpenGL/Creator/Texture2DCreator.hh>


#include <engine/events/MouseEvent.hpp>
#include <engine/events/KeyboardEvent.hpp>

#include <engine/graphics/BloomPostFX.hpp>

void Console::startup() {
  consolePassId = m_renderer->getRenderPassId("Console"_sh);

  auto wallHitSound = m_audio->createSound("wallHit.wav", SoundMode::MODE_3D);

  soundSource = m_sceneGraph->create();
  soundSourceTransform = soundSource.assign<Transform>();
  soundSourceAudio = soundSource.assign<SoundSource>(wallHitSound);
  soundSourceAudio->setVolume(3);

  auto textTexture =
    Texture2DFileManager::the()->get(Texture2DCreator("checkerboard.png"));

  auto backgroundGeom = Geometry{ VertexArrayObjectCreator("plane.obj").create() };


  auto backgroundShader =
    ShaderProgramFileManager::the()->get(ShaderProgramCreator("console/Background")
      .attributeLocations(backgroundGeom.vao->getAttributeLocations())
      .fragmentDataLocations(m_renderer->getGBufferLocations()));

  background = m_sceneGraph->create();
  backgroundDrawable = background.assign<Drawable>(backgroundGeom,
    Material{ glm::vec4{ 1, 1, 1, 1 }, glm::vec4{ 1, 1, 1, 1 } * 0.05,
    textTexture, nullptr, nullptr, nullptr, backgroundShader,
    false, RenderQueue::OPAQUE, GL_BACK },
    0, consolePassId);

  auto backgroundTransform = background.assign<Transform>();
  backgroundTransform->position = { 0, 0, -10 };
  backgroundTransform->scale = { 10, 5, 1 };

  auto textGeom = Geometry{ VertexArrayObjectCreator("title_text.obj").create() };

  auto textShader =
    ShaderProgramFileManager::the()->get(ShaderProgramCreator("console/Title")
          .attributeLocations(textGeom.vao->getAttributeLocations())
          .fragmentDataLocations(m_renderer->getGBufferLocations()));

  text = m_sceneGraph->create();
  textDrawable = text.assign<Drawable>(textGeom,
                        Material{glm::vec4{1, 1, 1, 1}, glm::vec4{1, 1, 1, 1},
                                 textTexture, nullptr, nullptr, nullptr, textShader,
                                 false, RenderQueue::OPAQUE, GL_BACK},
                        0, consolePassId);


  auto textTransform = text.assign<Transform>();
  textTransform->position = { 0, 1, -5 };
  textTransform->scale = { 0.7, 0.7, 0.7 };

  auto aGeom = Geometry{ VertexArrayObjectCreator("letters/letter_a.obj").create() };
  auto letterA = m_sceneGraph->create();
  letterADrawable = letterA.assign<Drawable>(aGeom,
    Material{ glm::vec4{ 1, 1, 1, 1 }, glm::vec4{ 1, 1, 1, 1 },
    textTexture, nullptr, nullptr, nullptr, textShader,
    false, RenderQueue::OPAQUE, GL_BACK },
    0, consolePassId);

  letterA.assign<Light>(glm::vec4(0, 0.2, 1.0, 5), glm::vec3(1, 0, 0),
    false, LightType::POINT, consolePassId);

  letterATransform = letterA.assign<Transform>();
  letterATransform->position = { -0.01356, -0.77264*0.7 + 1, -5 };
  letterATransform->scale = { 0.7, 0.7, 0.7 };

  textDrawable->material.emissiveColor = glm::vec4(0);
  letterADrawable->material.emissiveColor = textDrawable->material.emissiveColor;

  shotGeom = Geometry{ VertexArrayObjectCreator("intro_shot.obj").create() };

  shotMaterial = Material{ glm::vec4{ 1, 1, 1, 1 }, glm::vec4{ 1, 1, 1, 1 },
    textTexture, nullptr, nullptr, nullptr, textShader,
    false, RenderQueue::OPAQUE, GL_BACK };

  for (char& c : enemyOrder) {

    if (enemyGeometries.find(std::string(1, c)) != enemyGeometries.end()) {
      continue;
    }

    std::string objName = "letters/letter_";
    objName += c;
    objName += ".obj";
    auto enemyGeom = Geometry{ VertexArrayObjectCreator(objName).create() };
    enemyGeometries[std::string(1, c)] = enemyGeom;
  }

  enemyMaterial = Material{ glm::vec4{ 1, 1, 1, 1 }, glm::vec4(3, .4, .2, 1),
    textTexture, nullptr, nullptr, nullptr, textShader,
    false, RenderQueue::OPAQUE, GL_BACK };

  auto planetGeom = Geometry{ VertexArrayObjectCreator("planet_wireframe.obj").create() };
  auto planetMaterial = Material{ glm::vec4{ 1, 1, 1, 1 }, glm::vec4(0.2, 1.0, 0.3, 1) * 2,
    textTexture, nullptr, nullptr, nullptr, textShader,
    false, RenderQueue::OPAQUE, GL_BACK };

  planet = m_sceneGraph->create();
  planetDrawable = planet.assign<Drawable>(planetGeom, planetMaterial, 0, consolePassId);

  planetTransform = planet.assign<Transform>();
  planetTransform->position = { 8, 0.2, -5 };
  planetTransform->scale = { 0.7, 0.7, 0.7 };
}

void Console::spawnShot(glm::dvec3 pos, glm::dvec3 vel, bool friendly) {
  auto newShot = m_sceneGraph->create();
  shotMaterial.emissiveColor = friendly ? glm::vec4(1, 5, 2, 1) : glm::vec4(5, 2, 2, 1);
  newShot.assign<Drawable>(shotGeom, shotMaterial, 0, consolePassId);
  auto trans = newShot.assign<Transform>();
  trans->position = pos;
  trans->scale = { 0.5, 0.5, 0.3 };

  trans->lastPosition = pos;

  auto shot = newShot.assign<Shot>();
  shot->friendly = friendly;
  shot->vel = vel;
  shot->lifetime = 0;

  shots.push_back(newShot);
}

void Console::updatePlanet(double dt) {
  planetTransform->position.x -= dt * (planetTransform->position.x - 1);

  planetRotation += dt * 0.3;
  planetTransform->rotation = glm::yawPitchRoll(planetRotation, 0.0, 0.0);
}

void Console::updateShots(double dt) {
  Transform::Handle trans;
  Shot::Handle shot;

  for (int i = shots.size() - 1; i >= 0; i--) {
    auto shotEntity = shots[i];

    if (!shotEntity.valid()) {
      shotEntity.destroy();
      shots.erase(shots.begin() + i);
      continue;
    }

    shotEntity.unpack<Transform, Shot>(trans, shot);
    shot->vel += glm::normalize(shot->vel) * 4 * dt;
    trans->position += shot->vel * dt;

    trans->scale.y = 1.5 * glm::pow(glm::min<float>(4.0/glm::abs(shot->vel.x), 1), 3) + 0.5 * glm::sin(shot->lifetime * 4);

    shot->lifetime += dt;

    Transform::Handle enemyTrans;
    glm::dvec2 thisPos = glm::dvec2{ trans->position };
    for (int j = enemies.size() - 1; j >= 0; j--) {
      enemies[j].unpack<Transform>(enemyTrans);
      glm::dvec2 enemyPos = glm::dvec2{ enemyTrans->position };

      if (glm::length(thisPos - enemyPos) < 0.5) {
        enemies[j].component<Enemy>()->lifes = 0;
        enemies[j].destroy();
        enemies.erase(enemies.begin() + j);

        shotEntity.destroy();
        shots.erase(shots.begin() + i);
        break;
      }
    }

    if (!shotEntity.valid()) continue;

    if (trans->position.x < -4 || trans->position.x > 5) {
      shotEntity.destroy();
      shots.erase(shots.begin() + i);
    }
  }
}

void Console::spawnEnemy(glm::dvec3 pos, glm::dvec3 vel) {
  auto newEnemy = m_sceneGraph->create();

  auto enemyGeom = enemyGeometries[std::string(1, enemyOrder.at(currentEnemy % enemyOrder.length()))];

  currentEnemy++;

  newEnemy.assign<Drawable>(enemyGeom, enemyMaterial, 0, consolePassId);
  auto trans = newEnemy.assign<Transform>();
  trans->position = pos;
  trans->scale = { 1, 1, 1 };

  trans->lastPosition = pos;

  auto enemy = newEnemy.assign<Enemy>();
  enemy->vel = vel;
  enemy->lifetime = 0;
  enemy->lifes = 1;

  enemies.push_back(newEnemy);
}

void Console::updateEnemies(double dt) {
  Transform::Handle trans;
  Enemy::Handle enemy;

  for (int i = enemies.size() - 1; i >= 0; i--) {
    auto enemyEntity = enemies[i];
    enemyEntity.unpack<Transform, Enemy>(trans, enemy);
    trans->position += enemy->vel * dt;

    if (trans->position.x < -6 || enemy->lifes == 0) {
      enemyEntity.destroy();
      enemies.erase(enemies.begin() + i);
      continue;
    }

    enemy->lifetime += dt;
  }
}

void Console::updateFFT() {
  float* data = nullptr;
  unsigned int length = 0;
  m_audio->getSpectrum(&data, &length);

  float max = 0.0001f;
  for (int i = 0; i < length; i++) {
    if (data[i] > max) max = data[i];
  }

  GLubyte* byteData = new GLubyte[length];
  for (int i = 0; i < length; i++) {
    byteData[i] = (GLubyte)(data[i] / max * 255);
  }

  SharedTextureData fftData = std::make_shared<TextureData>();
  fftData->setWidth(length);
  fftData->setHeight(1);
  fftData->setData(byteData);
  fftData->setFormat(GL_RED);
  fftData->setType(GL_UNSIGNED_BYTE);

  auto fftTex = std::make_shared<Texture2D>(GL_R8);
  fftTex->setMinFilter(GL_LINEAR);
  fftTex->setMagFilter(GL_LINEAR);
  fftTex->setImageData(fftData);

  backgroundDrawable->material.mainTexture = fftTex;

  float posPercentage = (letterATransform->position.x + 3.2) / 4.5;
  m_audio->setLowPassCutoff(500 + posPercentage * 3000);
}

bool Console::update(const SimulateEvent &e, const bool* keyTable) {

  double dt = e.dt;

  updateShots(dt);
  updateEnemies(dt);
  updateFFT();

  switch (state) {
  case State::FadingIn:
    if (timer > FADE_IN_TIME) {
      state = State::FadingTextOut;
      textDrawable->material.emissiveColor = glm::vec4(0.7);
      letterADrawable->material.emissiveColor = glm::vec4(1.6);
    }
    textDrawable->material.emissiveColor = glm::vec4(timer / FADE_IN_TIME * 0.7);
    letterADrawable->material.emissiveColor = glm::vec4(timer / FADE_IN_TIME * 1.6);
    break;
  case State::FadingTextOut:
    if (timer > FADE_OUT_TIME) {
      state = State::MovingA;
      textDrawable->material.emissiveColor = glm::vec4(0);
      text.destroy();

      aStartPos = letterATransform->position;
      aTargetPos = glm::dvec3{-3, 0.3, -5};

      aStartRot = glm::quat_cast(letterATransform->rotation);
      aTargetRot = glm::quat(glm::vec3(0, 0, -M_PI / 2));
      break;
    }

    textDrawable->material.emissiveColor = glm::vec4(
        1.0f - (timer - FADE_IN_TIME) / (FADE_OUT_TIME - FADE_IN_TIME));
    break;
  case State::MovingA: {
    if (timer > MOVE_A_TIME) {
      state = State::Playing;
      letterATransform->position = aTargetPos;
      letterATransform->rotation = glm::mat4_cast(aTargetRot);
      letterADrawable->material.emissiveColor = glm::vec4(0.2, 1.0, 0.3, 1) * 2;
      break;
    }

    float interpolationTime = timer - FADE_OUT_TIME;
    float totalInterpolationTime = MOVE_A_TIME - FADE_OUT_TIME;
    double interpolationPercent =
        1.0 - glm::pow(1 - interpolationTime / totalInterpolationTime, 2);
    letterATransform->position =
        aStartPos + (aTargetPos - aStartPos) * interpolationPercent;

    interpolationPercent =
        1.0 - glm::pow(1 - interpolationTime / totalInterpolationTime, 5);
    glm::quat rot =
        glm::slerp(aStartRot, aTargetRot, (float)interpolationPercent);
    letterATransform->rotation = glm::mat4_cast(rot);
    break;
  }

  case State::Playing:

    glm::vec3 moveDir = { };
    if (keyTable[SDL_SCANCODE_W]) moveDir.y++;
    if (keyTable[SDL_SCANCODE_S]) moveDir.y--;
    if (keyTable[SDL_SCANCODE_D]) moveDir.x++;
    if (keyTable[SDL_SCANCODE_A]) moveDir.x--;

    if (keyTable[SDL_SCANCODE_SPACE] && timeSinceLastShot > 0.3) {
      spawnShot(letterATransform->position + glm::dvec3(0.5, 0, 0), glm::dvec3(4 + glm::max<float>(playerSpeed.x, 0), 0, 0), true);
      timeSinceLastShot = 0;
    }
    timeSinceLastShot += dt;

    nextEnemySpawnTime -= dt;
    if (nextEnemySpawnTime <= 0 && currentEnemy < enemyOrder.length()) {
      spawnEnemy({ 5, -1.4 + ((float)std::rand() / RAND_MAX) * (2 + 1.4), -5 }, { -2, 0 ,0 });
      nextEnemySpawnTime = ((float)std::rand() / RAND_MAX) * 1 + 0.5;
    }

    if (currentEnemy == enemyOrder.length() && nextEnemySpawnTime <= 0) {
      updatePlanet(dt);

      if (glm::length(letterATransform->position - planetTransform->position) < 1) return true;
    }

    playerSpeed += moveDir * 20 * dt;

    auto newPos = letterATransform->position + playerSpeed * dt;

    if (newPos.x < -3.2) {
      playerSpeed.x *= -0.9f;
      soundSourceTransform->position = -newPos * 0.1;
      soundSourceAudio->setVolume(playerSpeed.length());
      soundSourceAudio->stop();
      soundSourceAudio->play();
    }

    if (newPos.y > 2.0 || newPos.y < -1.4) {
      soundSourceTransform->position = -newPos * 0.1;
      soundSourceAudio->setVolume(playerSpeed.length());
      soundSourceAudio->stop();
      soundSourceAudio->play();

      playerSpeed.y *= -0.9f;
    }

    Transform::Handle enemyTrans;
    glm::dvec2 thisPos = glm::dvec2{ newPos };
    for (int j = enemies.size() - 1; j >= 0; j--) {
      enemies[j].unpack<Transform>(enemyTrans);
      glm::dvec2 enemyPos = glm::dvec2{ enemyTrans->position };

      glm::dvec3 dir = glm::dvec3{ thisPos - enemyPos, 0 };
      if (glm::length(dir) < 0.75) {
        playerSpeed = glm::reflect(playerSpeed, glm::normalize(dir)) * 0.9;
      }
    }

    letterATransform->scale.y = 0.7f + glm::clamp(playerSpeed.x * 0.1, -0.15, 0.15);
    letterATransform->scale.x = 0.7f - glm::clamp(playerSpeed.x * 0.1, -0.15, 0.15);

    letterATransform->position += playerSpeed * dt;
    playerSpeed -= playerSpeed * 5 * dt;

    break;
  }


  timer += e.dt;
  return false;
}

void Console::pause() {}

void Console::play() {}
