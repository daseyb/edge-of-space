#include <engine/scene/scenes/OrbitsScene.hpp>

#include <engine/ui/UISystem.hpp>

#include <ACGL/ACGL.hh>

#include <ACGL/OpenGL/Objects.hh>
#include <ACGL/Base/Settings.hh>
#include <ACGL/Math/Math.hh>
#include <ACGL/OpenGL/Data/TextureLoadStore.hh>
#include <ACGL/OpenGL/Managers.hh>

#include <ACGL/OpenGL/Creator/ShaderProgramCreator.hh>
#include <ACGL/OpenGL/Creator/VertexArrayObjectCreator.hh>
#include <ACGL/OpenGL/Creator/Texture2DCreator.hh>

#include <engine/scene/Transform.hpp>
#include <engine/scene/Drawable.hpp>
#include <engine/scene/Planet.hpp>

#include <engine/events/MouseEvent.hpp>
#include <engine/events/KeyboardEvent.hpp>

#include <engine/graphics/BloomPostFX.hpp>

using namespace ACGL::OpenGL;
using namespace ACGL::Base;
using namespace ACGL::Utils;

bool OrbitsScene::startup() {
  if (!Scene::startup()) {
    return false;
  }

  RESOLVE_DEPENDENCY(m_orbitals);
  RESOLVE_DEPENDENCY(m_player);

  //m_renderer->addEffect<BloomPostFX>();

  // load the geometry of the stanford bunny and build a VAO:
  auto vaoTeapot = VertexArrayObjectCreator("teapot.obj").create();

  // load a test scene
  auto vaoCube = VertexArrayObjectCreator("cube.obj").create();
  auto vaoSun = VertexArrayObjectCreator("uvsphere.obj").create();
  auto vaoTestScene = VertexArrayObjectCreator("test_scene.obj").create();

  // load a texture:
  auto checkboardTexture = Texture2DFileManager::the()->get(Texture2DCreator("checkerboard.png"));
  auto testTransparencyTexture = Texture2DFileManager::the()->get(Texture2DCreator("transparency_test.png"));

  // look up all shader files starting with 'PBR' and build a ShaderProgram from it:
  auto pbrShader = ShaderProgramCreator("PBR")
    .attributeLocations(vaoTeapot->getAttributeLocations())
    .fragmentDataLocations(m_renderer->getGBufferLocations()).create();

  // Create geometry objects that point to the previously initialized vaos
  Geometry geom1 = { vaoSun };
  Geometry geom2 = { vaoTeapot };

  // Create a material that uses the loaded shader program
  Material sunMaterial = { glm::vec4{ 1, 1, 1, 1 }, glm::vec4{ 2, 2, 1, 1 }, checkboardTexture, nullptr, nullptr, nullptr, pbrShader, false, RenderQueue::OPAQUE, GL_BACK };
  Material transparentMat = { glm::vec4{ 1, 1, 1, 1 }, glm::vec4{ 0, 0, 0, 1 }, testTransparencyTexture, nullptr, nullptr, nullptr, pbrShader, false, RenderQueue::TRANSPARENT, GL_BACK };

  // Let's create a placeholder sun
  auto scene = m_sceneGraph->create();
  // Add a transform component to it so we are able to position it in space
  auto sceneTransform = scene.assign<Transform>();
  // Add a Drawable component to it so the renderer has something to draw
  auto sceneDrawable = scene.assign<Drawable>(Geometry{ vaoTestScene }, transparentMat);
  float scale = m_orbitals->solarRadius * m_orbitals->scaleFactor * 0.1;
  sceneTransform->scale = glm::dvec3(scale);
  sceneTransform->position = glm::dvec3(0, -600, 0);
  sceneDrawable->visible = false;


  auto light = m_sceneGraph->create();
  light.assign<Transform>()->position = glm::dvec3(0, -200, 0);
  auto lightComp = light.assign<Light>(glm::vec4(1, 1, 1, 2000), glm::vec3(0, -1, 0), true);

  // Let's create a placeholder sun
  sun = m_sceneGraph->create();
  // Add a transform component to it so we are able to position it in space
  auto sunTransform = sun.assign<Transform>();
  // Add a Drawable component to it so the renderer has something to draw
  sun.assign<Drawable>(geom1, sunMaterial);
  sunTransform->scale = glm::dvec3(scale);
  sun.assign<Light>(glm::vec4(1, 1, 1, 2), glm::vec3(1, 0, 0), false, LightType::DIRECTIONAL);

  // create a planet! 
  // Mass in solar masses, Radius in solar radii, Eccentricity, Semimajor axis, Inclination, Ascending Node, Arg. of Periapsis, time at perihelion

  //																mass              radius            e         a        i          N          w
  mercury = m_orbitals->addPlanet(sunTransform, "Mercury", 0.000000165956, 0.003505316091954, 0.205633, 0.387098, 0.1222500, 0.84153, 0.50768, 0);
  venus = m_orbitals->addPlanet(sunTransform, "Venus", 0.00000243522, 0.008695402298851, 0.006778, 0.723330, 0.0592500, 1.33679, 0.95717, 0);
  earth = m_orbitals->addPlanet(sunTransform, "Earth", 0.000002988, 0.009153735632184, 0.016713, 1.000000, 0.0000000, 0.00000, 4.93533, 0);
  mars = m_orbitals->addPlanet(sunTransform, "Mars", 0.000000319716, 0.004870689655172, 0.093396, 1.523688, 0.0322851, 0.02333, 4.99858, 0);
  jupiter = m_orbitals->addPlanet(sunTransform, "Jupiter", 0.000954265748, 0.1004468390805, 0.048482, 5.202560, 0.0227500, 1.75150, 4.77905, 0);
  saturn = m_orbitals->addPlanet(sunTransform, "Saturn", 0.00028386, 0.08366666666667, 0.055580, 9.554750, 0.0434412, 1.98230, 5.92169, 0);
  uranus = m_orbitals->addPlanet(sunTransform, "Uranus", 0.00004344552, 0.03643965517241, 0.047292, 19.18176, 0.0134948, 1.29060, 1.68516, 0);
  neptune = m_orbitals->addPlanet(sunTransform, "Neptune", 0.0000512442, 0.03537643678161, 0.008598, 30.05814, 0.0002355, 2.29810, 4.76243, 0);


  // Load a sound and enable 3D positioning for it
  auto testSound = m_audio->createSound("test.wav", SoundMode::MODE_3D);

  // Attach a component to the entity that plays the loaded sound 
  // A sound can be referenced by multiple sound sources
  soundSource = sun.assign<SoundSource>(testSound);

  // Subscribe to the UI drawing event.
  // This is called once per frame
  m_events->subscribe<"DrawUI"_sh>([this]() {


    ImGui::Begin("Sound Control", 0, ImGuiWindowFlags_::ImGuiWindowFlags_AlwaysAutoResize);
    if (ImGui::Button("Pause Sound", glm::vec2(100, 20))) {
      soundSource->pause();
    }

    if (ImGui::Button("Play Sound", glm::vec2(100, 20))) {
      soundSource->play();
    }

    if (ImGui::Button("Stop Sound", glm::vec2(100, 20))) {
      soundSource->stop();
    }

    ImGui::Text("Playback State: %d", soundSource->getState());
    ImGui::End();

    ImGui::Begin("Camera Control", 0,
      ImGuiWindowFlags_::ImGuiWindowFlags_AlwaysAutoResize);

		float r;
    if (ImGui::Button("Sun/Global", glm::vec2(100, 20))) {
      // Reset the camera to sun coords
      m_player->attachToParent(sun);
			r = sun.component<Transform>()->scale.x;
      m_player->setPosition(glm::dvec3(0, 0, r * 1.2));
      m_player->setRotation(glm::dmat4());
    }


    if (ImGui::Button("Earth", glm::vec2(100, 20))) {
      m_player->attachToParent(earth);
			r = earth.component<Transform>()->scale.x;
			m_player->setPosition(glm::dvec3(0, 0, r * 1.2));
      m_player->setRotation(glm::dmat4());
    }

    if (ImGui::Button("Jupiter", glm::vec2(100, 20))) {
      m_player->attachToParent(jupiter);
			r = jupiter.component<Transform>()->scale.x;
			m_player->setPosition(glm::dvec3(0, 0, r * 1.2));
      m_player->setRotation(glm::dmat4());
    }

    if (ImGui::Button("Neptune", glm::vec2(100, 20))) {
      m_player->attachToParent(neptune);
			r = neptune.component<Transform>()->scale.x;
			m_player->setPosition(glm::dvec3(0, 0, r * 1.2));
      m_player->setRotation(glm::dmat4());
    }

    if (ImGui::Button("Trajectories", glm::vec2(100, 20))) {
      m_orbitals->showTrajectories();
    }

    glm::dvec3 pos = m_player->getPosition();
    ImGui::Text("(%.1f, %.1f, %.1f)", pos.x, pos.y, pos.z);
    ImGui::Text("N: %s", m_player->getParentName().c_str());

    ImGui::Text("");
    ImGui::Text("Fly next to a planet");
    ImGui::Text("to follow it. Press");
    ImGui::Text("Shift to go faster.");
    ImGui::Text("Space to move up");
    ImGui::End();
  });

  return true;
}

void OrbitsScene::shutdown() {
}
