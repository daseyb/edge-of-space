#include <engine/scene/PlayerSystem.hpp>
#include <engine/scene/OrbitalSimulationSystem.hpp>

#include "glm/ext.hpp"


glm::dvec3 rotate(glm::dvec3 vec, glm::dmat4 mat);

glm::vec3 mod289(glm::vec3  x)
{
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

glm::vec4 mod289(glm::vec4 x)
{
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

glm::vec4 permute(glm::vec4 x)
{
  return mod289(((x*34.0) + glm::vec4(1.0, 1.0, 1.0, 1.0))*x);
}

glm::vec4 taylorInvSqrt(glm::vec4 r)
{
  return glm::vec4(1.79284291400159, 1.79284291400159, 1.79284291400159, 1.79284291400159) - 0.85373472095314 * r;
}

glm::vec3  fade(glm::vec3  t) {
  return t*t*t*(t*(t*6.0 - glm::vec3(15.0, 15.0, 15.0)) + glm::vec3(10.0, 10.0, 10.0));
}

// Classic Perlin noise
float cnoise(glm::vec3  P)
{
  glm::vec3  Pi0 = floor(P); // Integer part for indexing
  glm::vec3  Pi1 = Pi0 + glm::vec3(1.0); // Integer part + 1
  Pi0 = mod289(Pi0);
  Pi1 = mod289(Pi1);
  glm::vec3  Pf0 = fract(P); // Fractional part for interpolation
  glm::vec3  Pf1 = Pf0 - glm::vec3(1.0); // Fractional part - 1.0
  glm::vec4 ix = glm::vec4(Pi0.x, Pi1.x, Pi0.x, Pi1.x);
  glm::vec4 iy = glm::vec4(Pi0.y, Pi0.y, Pi1.y, Pi1.y);
  glm::vec4 iz0 = glm::vec4(Pi0.z, Pi0.z, Pi0.z, Pi0.z);
  glm::vec4 iz1 = glm::vec4(Pi1.z, Pi1.z, Pi1.z, Pi1.z);

  glm::vec4 ixy = permute(permute(ix) + iy);
  glm::vec4 ixy0 = permute(ixy + iz0);
  glm::vec4 ixy1 = permute(ixy + iz1);

  glm::vec4 gx0 = ixy0 * (1.0 / 7.0);
  glm::vec4 gy0 = fract(floor(gx0) * (1.0 / 7.0)) - glm::vec4(0.5, 0.5, 0.5, 0.5);
  gx0 = fract(gx0);
  glm::vec4 gz0 = glm::vec4(0.5) - abs(gx0) - abs(gy0);
  glm::vec4 sz0 = step(gz0, glm::vec4(0.0));
  gx0 -= sz0 * (glm::step(glm::vec4(0.0, 0, 0, 0), gx0) - glm::vec4(0.5, 0.5, 0.5, 0.5));
  gy0 -= sz0 * (step(glm::vec4(0.0, 0, 0, 0), gy0) - glm::vec4(0.5, 0.5, 0.5, 0.5));

  glm::vec4 gx1 = ixy1 * (1.0 / 7.0);
  glm::vec4 gy1 = fract(floor(gx1) * (1.0 / 7.0)) - glm::vec4(0.5, 0.5, 0.5, 0.5);
  gx1 = fract(gx1);
  glm::vec4 gz1 = glm::vec4(0.5) - abs(gx1) - abs(gy1);
  glm::vec4 sz1 = step(gz1, glm::vec4(0.0));
  gx1 -= sz1 * (step(glm::vec4(0.0, 0, 0, 0), gx1) - glm::vec4(0.5, 0.5, 0.5, 0.5));
  gy1 -= sz1 * (step(glm::vec4(0.0, 0, 0, 0), gy1) - glm::vec4(0.5, 0.5, 0.5, 0.5));

  glm::vec3  g000 = glm::vec3(gx0.x, gy0.x, gz0.x);
  glm::vec3  g100 = glm::vec3(gx0.y, gy0.y, gz0.y);
  glm::vec3  g010 = glm::vec3(gx0.z, gy0.z, gz0.z);
  glm::vec3  g110 = glm::vec3(gx0.w, gy0.w, gz0.w);
  glm::vec3  g001 = glm::vec3(gx1.x, gy1.x, gz1.x);
  glm::vec3  g101 = glm::vec3(gx1.y, gy1.y, gz1.y);
  glm::vec3  g011 = glm::vec3(gx1.z, gy1.z, gz1.z);
  glm::vec3  g111 = glm::vec3(gx1.w, gy1.w, gz1.w);

  glm::vec4 norm0 = taylorInvSqrt(glm::vec4(dot(g000, g000), dot(g010, g010), dot(g100, g100), dot(g110, g110)));
  g000 *= norm0.x;
  g010 *= norm0.y;
  g100 *= norm0.z;
  g110 *= norm0.w;
  glm::vec4 norm1 = taylorInvSqrt(glm::vec4(dot(g001, g001), dot(g011, g011), dot(g101, g101), dot(g111, g111)));
  g001 *= norm1.x;
  g011 *= norm1.y;
  g101 *= norm1.z;
  g111 *= norm1.w;

  float n000 = dot(g000, Pf0);
  float n100 = dot(g100, glm::vec3(Pf1.x, Pf0.y, Pf0.z));
  float n010 = dot(g010, glm::vec3(Pf0.x, Pf1.y, Pf0.z));
  float n110 = dot(g110, glm::vec3(Pf1.x, Pf1.y, Pf0.z));
  float n001 = dot(g001, glm::vec3(Pf0.x, Pf0.y, Pf1.z));
  float n101 = dot(g101, glm::vec3(Pf1.x, Pf0.y, Pf1.z));
  float n011 = dot(g011, glm::vec3(Pf0.x, Pf1.y, Pf1.z));
  float n111 = dot(g111, Pf1);

  glm::vec3 fade_xyz = fade(Pf0);
  glm::vec4 n_z = glm::mix(glm::vec4(n000, n100, n010, n110), glm::vec4(n001, n101, n011, n111), fade_xyz.z);
  glm::vec2 n_yz = glm::mix(glm::vec2(n_z.x, n_z.y), glm::vec2(n_z.z, n_z.w), fade_xyz.y);
  float n_xyz = glm::mix(n_yz.x, n_yz.y, fade_xyz.x);
  return 2.2 * n_xyz;
}

// Classic Perlin noise, periodic variant
float pnoise(glm::vec3  P, glm::vec3  rep)
{
  glm::vec3  Pi0 = mod(floor(P), rep); // Integer part, modulo period
  glm::vec3  Pi1 = mod(Pi0 + glm::vec3(1.0), rep); // Integer part + 1, mod period
  Pi0 = mod289(Pi0);
  Pi1 = mod289(Pi1);
  glm::vec3  Pf0 = fract(P); // Fractional part for interpolation
  glm::vec3  Pf1 = Pf0 - glm::vec3(1.0); // Fractional part - 1.0
  glm::vec4 ix = glm::vec4(Pi0.x, Pi1.x, Pi0.x, Pi1.x);
  glm::vec4 iy = glm::vec4(Pi0.y, Pi0.y, Pi1.y, Pi1.y);
  glm::vec4 iz0 = glm::vec4(Pi0.z, Pi0.z, Pi0.z, Pi0.z);
  glm::vec4 iz1 = glm::vec4(Pi1.z, Pi1.z, Pi1.z, Pi1.z);

  glm::vec4 ixy = permute(permute(ix) + iy);
  glm::vec4 ixy0 = permute(ixy + iz0);
  glm::vec4 ixy1 = permute(ixy + iz1);

  glm::vec4 gx0 = ixy0 * (1.0 / 7.0);
  glm::vec4 gy0 = fract(floor(gx0) * (1.0 / 7.0)) - glm::vec4(0.5, 0.5, 0.5, 0.5);
  gx0 = fract(gx0);
  glm::vec4 gz0 = glm::vec4(0.5) - abs(gx0) - abs(gy0);
  glm::vec4 sz0 = step(gz0, glm::vec4(0.0));
  gx0 -= sz0 * (step(glm::vec4(0.0, 0.0, 0.0, 0.0), gx0) - glm::vec4(0.5, 0.5, 0.5, 0.5));
  gy0 -= sz0 * (step(glm::vec4(0.0, 0.0, 0.0, 0.0), gy0) - glm::vec4(0.5, 0.5, 0.5, 0.5));

  glm::vec4 gx1 = ixy1 * (1.0 / 7.0);
  glm::vec4 gy1 = fract(floor(gx1) * (1.0 / 7.0)) - glm::vec4(0.5, 0.5, 0.5, 0.5);
  gx1 = fract(gx1);
  glm::vec4 gz1 = glm::vec4(0.5) - abs(gx1) - abs(gy1);
  glm::vec4 sz1 = step(gz1, glm::vec4(0.0));
  gx1 -= sz1 * (step(glm::vec4(0.0, 0.0, 0.0, 0.0), gx1) - glm::vec4(0.5, 0.5, 0.5, 0.5));
  gy1 -= sz1 * (step(glm::vec4(0.0, 0.0, 0.0, 0.0), gy1) - glm::vec4(0.5, 0.5, 0.5, 0.5));

  glm::vec3  g000 = glm::vec3(gx0.x, gy0.x, gz0.x);
  glm::vec3  g100 = glm::vec3(gx0.y, gy0.y, gz0.y);
  glm::vec3  g010 = glm::vec3(gx0.z, gy0.z, gz0.z);
  glm::vec3  g110 = glm::vec3(gx0.w, gy0.w, gz0.w);
  glm::vec3  g001 = glm::vec3(gx1.x, gy1.x, gz1.x);
  glm::vec3  g101 = glm::vec3(gx1.y, gy1.y, gz1.y);
  glm::vec3  g011 = glm::vec3(gx1.z, gy1.z, gz1.z);
  glm::vec3  g111 = glm::vec3(gx1.w, gy1.w, gz1.w);

  glm::vec4 norm0 = taylorInvSqrt(glm::vec4(dot(g000, g000), dot(g010, g010), dot(g100, g100), dot(g110, g110)));
  g000 *= norm0.x;
  g010 *= norm0.y;
  g100 *= norm0.z;
  g110 *= norm0.w;
  glm::vec4 norm1 = taylorInvSqrt(glm::vec4(dot(g001, g001), dot(g011, g011), dot(g101, g101), dot(g111, g111)));
  g001 *= norm1.x;
  g011 *= norm1.y;
  g101 *= norm1.z;
  g111 *= norm1.w;

  float n000 = dot(g000, Pf0);
  float n100 = dot(g100, glm::vec3(Pf1.x, Pf0.y, Pf0.z));
  float n010 = dot(g010, glm::vec3(Pf0.x, Pf1.y, Pf0.z));
  float n110 = dot(g110, glm::vec3(Pf1.x, Pf1.y, Pf0.z));
  float n001 = dot(g001, glm::vec3(Pf0.x, Pf0.y, Pf1.z));
  float n101 = dot(g101, glm::vec3(Pf1.x, Pf0.y, Pf1.z));
  float n011 = dot(g011, glm::vec3(Pf0.x, Pf1.y, Pf1.z));
  float n111 = dot(g111, Pf1);

  glm::vec3  fade_xyz = fade(Pf0);
  glm::vec4 n_z = glm::mix(glm::vec4(n000, n100, n010, n110), glm::vec4(n001, n101, n011, n111), fade_xyz.z);
  glm::vec2 n_yz = glm::mix(glm::vec2(n_z.x, n_z.y), glm::vec2(n_z.z, n_z.w), fade_xyz.y);
  float n_xyz = glm::mix(n_yz.x, n_yz.y, fade_xyz.x);
  return 2.2 * n_xyz;
}


float snoise(glm::vec3 v)
{
  const glm::vec2  C = glm::vec2(1.0 / 6.0, 1.0 / 3.0);
  const glm::vec4  D = glm::vec4(0.0, 0.5, 1.0, 2.0);

  // First corner
  glm::vec3 i = floor(v + glm::dot(v, glm::vec3(C.y, C.y, C.y)));
  glm::vec3 x0 = v - i + glm::dot(i, glm::vec3(C.x, C.x, C.x));

  // Other corners
  glm::vec3 g = glm::step(x0, x0);
  glm::vec3 l = glm::vec3(1.0, 1.0, 1.0) - g;
  glm::vec3 i1 = glm::min(g, glm::vec3(l.z, l.x, l.y));
  glm::vec3 i2 = glm::max(g, glm::vec3(l.z, l.x, l.y));

  //   x0 = x0 - 0.0 + 0.0 * C.xxx;
  //   x1 = x0 - i1  + 1.0 * C.xxx;
  //   x2 = x0 - i2  + 2.0 * C.xxx;
  //   x3 = x0 - 1.0 + 3.0 * C.xxx;
  glm::vec3 x1 = x0 - i1 + glm::vec3(C.x, C.x, C.x);
  glm::vec3 x2 = x0 - i2 + glm::vec3(C.y, C.y, C.y); // 2.0*C.x = 1/3 = C.y
  glm::vec3 x3 = x0 - glm::vec3(D.y, D.y, D.y);      // -1.0+3.0*C.x = -0.5 = -D.y

                             // Permutations
  i = mod289(i);
  glm::vec4 p = permute(permute(permute(
    i.z + glm::vec4(0.0, i1.z, i2.z, 1.0))
    + i.y + glm::vec4(0.0, i1.y, i2.y, 1.0))
    + i.x + glm::vec4(0.0, i1.x, i2.x, 1.0));

  // Gradients: 7x7 points over a square, mapped onto an octahedron.
  // The ring size 17*17 = 289 is close to a multiple of 49 (49*6 = 294)
  float n_ = 0.142857142857; // 1.0/7.0
  glm::vec3  ns = n_ * glm::vec3(D.w, D.y, D.z) - glm::vec3(D.x, D.z, D.x);

  glm::vec4 j = p - 49.0 * glm::floor(p * ns.z * ns.z);  //  mod(p,7*7)

  glm::vec4 x_ = glm::floor(j * ns.z);
  glm::vec4 y_ = glm::floor(j - 7.0 * x_);    // mod(j,N)

  glm::vec4 x = x_ *ns.x + glm::vec4(ns.y, ns.y, ns.y, ns.y);
  glm::vec4 y = y_ *ns.x + glm::vec4(ns.y, ns.y, ns.y, ns.y);
  glm::vec4 h = glm::vec4(1.0, 1.0, 1.0, 1.0) - glm::abs(x) - glm::abs(y);

  glm::vec4 b0 = glm::vec4(x.x, x.y, y.x, y.y);
  glm::vec4 b1 = glm::vec4(x.z, x.w, y.z, y.w);

  //glm::vec4 s0 = glm::vec4(lessThan(b0,0.0))*2.0 - 1.0;
  //glm::vec4 s1 = glm::vec4(lessThan(b1,0.0))*2.0 - 1.0;
  glm::vec4 s0 = glm::floor(b0)*2.0 + glm::vec4(1.0, 1.0, 1.0, 1.0);
  glm::vec4 s1 = glm::floor(b1)*2.0 + glm::vec4(1.0, 1.0, 1.0, 1.0);
  glm::vec4 sh = -glm::step(h, glm::vec4(0.0));

  glm::vec4 a0 = glm::vec4(b0.x, b0.z, b0.y, b0.w) + glm::vec4(s0.x, s0.z, s0.y, s0.w) * glm::vec4(sh.x, sh.x, sh.y, sh.y);
  glm::vec4 a1 = glm::vec4(b1.x, b1.z, b1.y, b1.w) + glm::vec4(s1.x, s1.z, s1.y, s1.w) * glm::vec4(sh.z, sh.z, sh.w, sh.w);

  glm::vec3 p0 = glm::vec3(a0.x, a0.y, h.x);
  glm::vec3 p1 = glm::vec3(a0.z, a0.w, h.y);
  glm::vec3 p2 = glm::vec3(a1.x, a0.y, h.z);
  glm::vec3 p3 = glm::vec3(a1.z, a1.w, h.w);

  //Normalise gradients
  glm::vec4 norm = taylorInvSqrt(glm::vec4(dot(p0, p0), dot(p1, p1), dot(p2, p2), dot(p3, p3)));
  p0 *= norm.x;
  p1 *= norm.y;
  p2 *= norm.z;
  p3 *= norm.w;

  // Mix final noise value
  glm::vec4 m = glm::max(glm::vec4(0.6, 0.6, 0.6, 0.6) - glm::vec4(dot(x0, x0), dot(x1, x1), dot(x2, x2), dot(x3, x3)), glm::vec4(0.0, 0.0, 0.0, 0.0));
  m = m * m;
  return 42.0 * dot(m*m, glm::vec4(dot(p0, x0), dot(p1, x1),
    dot(p2, x2), dot(p3, x3)));
}


float fbm_3d(glm::vec3 p, int fOct, float freq, float weight = 0.5) {
  float fValue = 0.0;
  for (int i = 0; i < fOct; i++) {
    fValue += snoise(freq * p) * weight;
    weight *= 0.5;
    freq *= 2.0;
  }
  return fValue;
}

bool PlayerSystem::startup() {

  RESOLVE_DEPENDENCY(m_settings)
	RESOLVE_DEPENDENCY(m_events);
	RESOLVE_DEPENDENCY(m_scene);
	RESOLVE_DEPENDENCY(m_renderer);
	RESOLVE_DEPENDENCY(m_audio);
	RESOLVE_DEPENDENCY(m_ui);
  RESOLVE_DEPENDENCY(m_orbitals);
	RESOLVE_DEPENDENCY(m_window);

  m_skyboxCamera = m_scene->create();
  skyboxCamTransform = m_skyboxCamera.assign<Transform>();
  skyboxCamTransform->position = glm::dvec3(0, 0, 0);
  m_skyboxCamera.assign<Camera>(50, 0.01f, 50);

	// Create an entity that is used to position the camera in the scene
	m_mainCamera = m_scene->create();
	cameraTransform = m_mainCamera.assign<Transform>();
	camera = m_mainCamera.assign<Camera>(50, 1, 20000000);
  camera->isMain = true;
	cameraTransform->position = glm::dvec3(0, 0, 25000);
  directionRot = cameraTransform->rotation;


  m_cockpitCamera = m_scene->create();
  cockpitCamTransform = m_cockpitCamera.assign<Transform>();
  cockpitCamTransform->position = glm::dvec3(0, 0, 0);
  m_cockpitCamera.assign<Camera>(50, 0.01f, 50);

  m_minimapCamera = m_scene->create();
  auto camTransform = m_minimapCamera.assign<Transform>();
  auto mmCam = m_minimapCamera.assign<Camera>(50, 0.1f, 200);
  mmCam->mode = ProjectionMode::Orthographic;
  camTransform->position = glm::vec3(0, 0, 0);

  auto quality = m_settings->getQualitySetting();

  auto defaultScreenSpaceSize = quality == QualitySetting::High ? ScreenSpaceSize::FULL : 
                                quality == QualitySetting::Medium ? ScreenSpaceSize::HALF : 
                                                                    ScreenSpaceSize::HALF;

  m_renderer->addRenderPass(m_skyboxCamera, "Skybox"_sh, defaultScreenSpaceSize);
  m_renderer->addRenderPass(m_mainCamera, "Main"_sh, defaultScreenSpaceSize);
  m_renderer->addRenderPass(m_cockpitCamera, "Cockpit"_sh, defaultScreenSpaceSize);
  m_renderer->addRenderPass(m_minimapCamera, "Minimap"_sh, ScreenSpaceSize::QUARTER);
  m_renderer->setRenderPassOnlyTexture("Minimap"_sh, true);

  playerIcon = m_scene->create();
  playerIcon.assign<Drawable>(m_orbitals->playerIconGeometry, m_orbitals->playerIconMaterial, 0, m_renderer->getRenderPassId("Minimap"_sh));

  auto itemTransform = playerIcon.assign<Transform>();
  itemTransform->scale = { 3.0, 3.0, 3.0 };
  itemTransform->position = glm::dvec3(0, 0, -70);
  
	m_audio->setListener(m_mainCamera);

	m_events->subscribe<KeyboardEvent>([this](const KeyboardEvent& e) { handleKeyboard(e); });
	m_events->subscribe<MouseEvent>([this](const MouseEvent& e) { handleMouse(e); });
	m_events->subscribe<SimulateEvent>([this](const SimulateEvent& e) { update(e.dt); });

  for (int i = 0; i < SDL_NUM_SCANCODES; i++) {
    m_keyState[i] = false;
  }

	return true;
}

void PlayerSystem::handleKeyboard(KeyboardEvent e) {
	if (e.originalEvent.key.repeat != 0) {
		return;
	}

  if (e.originalEvent.key.keysym.scancode < SDL_NUM_SCANCODES && e.originalEvent.key.keysym.scancode >= 0) {
    m_keyState[e.originalEvent.key.keysym.scancode] = e.originalEvent.key.type == SDL_KEYDOWN;
  }

	switch (e.originalEvent.key.keysym.scancode) {
	case SDL_SCANCODE_ESCAPE:
		// Make the mouse freely movable on escape
		if (e.originalEvent.key.type == SDL_KEYDOWN) {
			if (SDL_GetRelativeMouseMode() == SDL_TRUE) {
				SDL_SetRelativeMouseMode(SDL_FALSE);
			}
			else SDL_SetRelativeMouseMode(SDL_TRUE);
		}
		break;
	default:
		break;
	}
}

void PlayerSystem::handleMouse(MouseEvent e) {
  if (!m_hasControl) {
    return;
  }

	switch (e.originalEvent.type) {
		// Capture the mouse when we click on the window
	case SDL_MOUSEBUTTONDOWN:
		// As long as we are not over any UI (because we might want to click on that)
		if (!ImGui::GetIO().WantCaptureMouse) {
			//SDL_SetRelativeMouseMode(SDL_TRUE);
			glm::vec2 winSize = m_window->getSize();			
			int x = e.originalEvent.button.x;
			int y = e.originalEvent.button.y;
			int fixedY = winSize.y - y - 1;			

			GLfloat depth = m_renderer->getDepthAtPixel(x, fixedY);

			if (depth == 1.0) {
				return; //infintiy!
			}

			glm::vec4 v = glm::vec4(0, 0, winSize.x, winSize.y);
			glm::dvec3 winCoords = glm::dvec3(x, fixedY, depth);

			Camera::Handle cam = m_mainCamera.component<Camera>();
			
			//glm::mat4 view = glm::lookAt(cameraTransform->position, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

			glm::dmat4 model = glm::translate(cameraTransform->thisGlobalTransform.pos) * static_cast<glm::dmat4>(glm::mat4_cast(cameraTransform->lastGlobalTransform.rot)) * glm::scale(cameraTransform->scale);

			glm::dmat4 proj = m_renderer->getProjectionMatrix();

			glm::dvec3 worldCoords = glm::unProject(winCoords, glm::inverse(model) , proj, v);

			std::cout << worldCoords.x << " " << worldCoords.y << " " << worldCoords.z << "\n"; 
      /*
      for (Entity e : m_scene->entities_with_components<Transform, Planet>()) {
				if (e.component<Planet>()->name == "Moon") {
					continue;
				}
				Transform::Handle p = e.component<Transform>();
				glm::dvec3 pos = p->position;
				if (length(p->position - worldCoords) <= p->scale.x + 100){
					attachToParent(e);
					setPosition(glm::dvec3(e.component<Transform>()->scale.x + 100, 0, 0));
				}
        
			}
      */
		}
		break;
	case SDL_MOUSEMOTION: {
		// If the mouse is captured
		if (SDL_GetRelativeMouseMode()) {
      mouseMove = glm::vec2(0, 0);
			// Rotate the camera based on the mouse movement
			mouseMove = glm::vec2(-(float)e.originalEvent.motion.xrel, -(float)e.originalEvent.motion.yrel);      
    }
		break;
	}    
	default:
		break;
	}
}

void PlayerSystem::update(float dt) {
  if (!m_hasControl) {
    return;
  }

	glm::dvec3 moveDir(0);
  glm::dvec2 rotDir(0);

	double speedMod = 1.0;

  // Move the camera based on WASD keyboard input
  if (!ImGui::GetIO().WantCaptureKeyboard) {

    if (m_keyState[SDL_SCANCODE_F2]) movementMode = 0;
    if (m_keyState[SDL_SCANCODE_F3]) movementMode = 1;
    if (m_keyState[SDL_SCANCODE_F4]) setPosition(glm::vec3(0, 0, 1000000));


    if (movementMode == 0) {
      if (m_keyState[SDL_SCANCODE_W]) moveDir -= glm::dvec3(0, 0, 1);
      if (m_keyState[SDL_SCANCODE_S]) moveDir += glm::dvec3(0, 0, 1);
      if (m_keyState[SDL_SCANCODE_A]) moveDir -= glm::dvec3(1, 0, 0);
      if (m_keyState[SDL_SCANCODE_D]) moveDir += glm::dvec3(1, 0, 0);

      if (m_keyState[SDL_SCANCODE_SPACE]) { moveDir += glm::dvec3(0, 1, 0); }
      if (m_keyState[SDL_SCANCODE_LSHIFT]) speedMod = 100;
      if (m_keyState[SDL_SCANCODE_LCTRL]) speedMod = 10000;     

      // TODO:
      velocity = 0;

      if (moveDir.x != 0 || moveDir.y || moveDir.z != 0) {
        moveDir = rotate(glm::normalize(moveDir), cameraTransform->rotation);
        cameraTransform->position += moveDir * static_cast<double>(dt) * speed * speedMod;
      }

    }
    else {

      int rollDir = 0;

      if (m_keyState[SDL_SCANCODE_LSHIFT]) speedMod = 500;

      if (m_keyState[SDL_SCANCODE_W] || m_keyState[SDL_SCANCODE_UP])    rotDir -= glm::dvec2(0, 1);
      if (m_keyState[SDL_SCANCODE_S] || m_keyState[SDL_SCANCODE_DOWN])  rotDir += glm::dvec2(0, 1);
      if (m_keyState[SDL_SCANCODE_A] || m_keyState[SDL_SCANCODE_LEFT])  rotDir += glm::dvec2(1, 0);
      if (m_keyState[SDL_SCANCODE_D] || m_keyState[SDL_SCANCODE_RIGHT]) rotDir -= glm::dvec2(1, 0);
      if (m_keyState[SDL_SCANCODE_Q]) rollDir = 1;
      if (m_keyState[SDL_SCANCODE_E]) rollDir = -1;

      if (m_keyState[SDL_SCANCODE_X] || m_keyState[SDL_SCANCODE_KP_PLUS]) velocity += acceleration * speedMod;
      if (m_keyState[SDL_SCANCODE_Z] || m_keyState[SDL_SCANCODE_Y] || m_keyState[SDL_SCANCODE_KP_MINUS]) velocity -= acceleration * speedMod;

      if (m_keyState[SDL_SCANCODE_SPACE]) {
        velocity = 0;
        rollDir = 0;
        rotDir = glm::dvec2(0, 0);
      }

      glm::dmat4 yprMatrix = glm::yawPitchRoll(rotDir.x * 0.01, rotDir.y * 0.01, rollDir * 0.01);       
      glm::dmat4 oldDir = directionRot;

      directionRot = directionRot * yprMatrix;
      
      auto dir = normalize( rotate(glm::dvec3(0, 0, -1), directionRot));
      cameraTransform->position += glm::normalize(glm::vec3(dir)) * static_cast<double>(dt) * velocity;


      auto up = rotate(glm::dvec3(0, 1, 0), glm::inverse(cockpitCamTransform->rotation));
      cockpitCamTransform->rotation = glm::rotate(cockpitCamTransform->rotation, mouseMove.x * 0.001, up);
      cockpitCamTransform->rotation = glm::rotate(cockpitCamTransform->rotation, mouseMove.y * 0.001, glm::dvec3(1, 0, 0));

      cameraTransform->rotation = directionRot * cockpitCamTransform->rotation;

      mouseMove = glm::vec2(0, 0);
    }
  }


  float targetFOV = 50 + (1.0 - exp(-glm::abs(velocity) * 0.001)) * 5 * glm::sign(velocity);
  float currentFOV = m_cockpitCamera.component<Camera>()->fov;

  m_cockpitCamera.component<Camera>()->fov = m_mainCamera.component<Camera>()->fov = currentFOV + (targetFOV - currentFOV) * dt * 5;

  skyboxCamTransform->rotation = cameraTransform->rotation;

	// Check if we have to move the player to a different coordinate system.

	double r;

	// Hack to check if the parent is not sun or we don't have a parent (FIXME)
	if (cameraTransform->parent.valid() && cameraTransform->parent->parent.valid()) {
    // we're following a planet!
		r = m_orbitals->solarToWorld(parentEntity.component<Planet>()->radius);

    double dist = glm::length(cameraTransform->position);

    // If we drift away from the planet, stop following its orbit:
		if (dist > r * 5 ) {
			cameraTransform->position += cameraTransform->parent->position;
      m_orbitals->minimapItems[parentEntity.id()].component<Drawable>()->material.emissiveColor = glm::vec4(1.0, 0.0, 0, 1.0);

			cameraTransform->parent = cameraTransform->parent->parent;
      camera->worldUp = glm::vec3(0, 1, 0);
            
      parentEntity = Entity(); //fixme
		}    

    // Do a rudimentary collision detection:    
    // We're in danger of colliding with the highest mountain:
    else if (dist <= r*1.5) {

      // If we come sufficiently close to a planet, alter the "up" vector of our camera/player so that we can fake some gravity
      // Push the up vector each frame towards the normal of the surface at that point.

      // Calculate the normalized direction from camera to model. This is the camera projected onto the surface of the sphere.
      // Ideally, we want to alter our up vector such that after some time it resembles the sphere normals at the projected camera point.
      glm::vec3 p = glm::normalize(cameraTransform->position);

      camera->worldUp = p;

      // Check if we're actually colliding with the noise at that position. This is a tiny bit tricky and expensive. 

      // Calculate the noise at that point:
      // TODO: Put the noise funtion somewhere where it makes more sense.
      float noise = fbm_3d(p, 4, 80.0) / 2.0 + 2 * fbm_3d(p, 2, 1.25);
      p = p + (p * noise * 0.008);
      p = p*r;
      if (dist <= glm::length(p) ) {
        // We're inside a planet. Due to that not being good for our health, let's get out quick:
        //cameraTransform->position = cameraTransform->lastPosition;
      }      
    }
	}

	// We're NOT following a planet, check if we have to attach to one.
	else {
		auto planets = m_scene->entities_with_components<Planet, Transform>();
		Planet::Handle planet;
		Transform::Handle transform;

		for (auto p : planets){
			p.unpack<Planet, Transform>(planet, transform);

			// Excluding moons FIXME
			if (planet->name != "Moon") {
				r = m_orbitals->solarToWorld(planet->radius);
				if (glm::length( cameraTransform->position - transform->position) <  r * 4.5){
					cameraTransform->position = cameraTransform->position - transform->position;
          cameraTransform->lastPosition = cameraTransform->lastPosition - transform->lastPosition;
					cameraTransform->parent = transform;
          velocity = 0;
          parentEntity = p;
          m_orbitals->minimapItems[parentEntity.id()].component<Drawable>()->material.emissiveColor = glm::vec4(0.0, 1.0, 0, 1.0);
          break;
				}
			}
		}
	}

  // Update the player icon:
  glm::dvec3 oldPos = cameraTransform->thisGlobalTransform.pos;
  oldPos = m_orbitals->worldToAU(oldPos);
  playerIcon.component<Transform>()->position = glm::dvec3(oldPos.x, oldPos.z, oldPos.y - 70);
  playerIcon.component<Transform>()->rotation = glm::rotate<double>(-M_PI / 2, glm::dvec3(1, 0, 0)) * directionRot;
}

void PlayerSystem::shutdown() {}

glm::dvec3 rotate(glm::dvec3 vec, glm::dmat4 mat) {
	auto temp = mat * glm::dvec4(vec.x, vec.y, vec.z, 0);
	return glm::dvec3(temp.x, temp.y, temp.z);
}


