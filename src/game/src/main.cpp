#include <iostream>
#include <memory>

#include <engine/events/EventSystem.hpp>
#include <engine/core/SettingsSystem.hpp>
#include <engine/core/WindowSystem.hpp>
#include <engine/audio/AudioSystem.hpp>
#include <engine/graphics/RendererSystem.hpp>
#include <engine/core/GameLoopSystem.hpp>
#include <engine/ui/UISystem.hpp>
#include <engine/scene/SceneGraphSystem.hpp>
#include <engine/scene/OrbitalSimulationSystem.hpp>
#include <engine/scene/PlayerSystem.hpp>

// Scenes we know about
#include <engine/scene/scenes/OrbitsScene.hpp>
#include <engine/scene/scenes/AtmosphereTestScene.hpp>

static std::unique_ptr<Scene> createScene(std::string sceneType, Context* context) {
  if (sceneType == "Orbits") {
    return std::unique_ptr<OrbitsScene>(new OrbitsScene(context, ""));
  } else if (sceneType == "AtmosphereTest") {
    return std::unique_ptr<AtmosphereTestScene>(new AtmosphereTestScene(context, ""));
  }
  // Add your scenes here
  else {
    return nullptr;
  }
}


int main(int argc, char *argv[]) {
  std::string configFile = "config/settings.json";
#if _DEBUG
  if (argc != 2) {
    std::cout << "Usage: <config file>" << std::endl;
    return -1;
  }
#endif

  // Set up the systems we want to use.
  // They depends on each other so we need to add them to the context in the right order
  // This also makes sure that dependencies are initialized before things that depend on them
  // If you try to initialize a system before it's dependencies you'll get an error message in the console
  Context context;
  SettingsSystem settings(&context, "data/", "textures/", "geometry/", "shader/", "sound/", configFile);
  WindowSystem window(&context, 1280, 720, false);
  EventSystem events(&context);
  RendererSystem renderer(&context);
  SceneGraphSystem sceneGraph(&context);
  AudioSystem audio(&context);
  UISystem ui(&context);
  GameLoopSystem gameLoop(&context, 60, 100);
  OrbitalSimulationSystem orbitals(&context);
  PlayerSystem player(&context); // Should be the last one

  // Call the startup functions of the systems above in the order they are listed.
  // This means it's safe to access other systems in a startup function as long
  // as you depend on them
  if (!context.startup()) {
    ACGL::Utils::error() << "Some systems failed to start up. Exiting!"
      << std::endl;
    return -1;
  }

  auto scene = createScene(settings.getDefaultScene(), &context);
  scene->startup();
    
  // Kickoff the gameloop
  // This is what actually runs the game
  gameLoop.run();

  // Shutdown the registered systems in reverse order
  context.shutdown();
  return 0;
}

