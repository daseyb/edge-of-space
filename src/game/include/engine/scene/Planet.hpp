#pragma once
#include <math.h>
#include <engine/scene/Transform.hpp>
#include <engine/scene/Entity.hpp>

#define PI	3.14159265358979323846  /* pi */

struct Planet : Component<Planet> {
	Entity parent;

	std::string name;

	double mass;					// in solar masses
	double radius;					// in solar radius

	glm::dvec3 heliocentricPosition;	// in au
	glm::dmat4 heliocentricRotation;

	// Orbital elements
	double eccentricity;		
	double semimajor;

	double inclination;		// Tilt
	double ascendingNode;	// Longitude of ascending node

	double timeAtPerihelion;
	double longitudePerihelion; // == periapsis

	double orbitalPeriod;
	double dailyMotion;

	double M0; // TODO (Where does the planet "start")

	Planet() {};

	// Mass in solar masses, Radius, Eccentricity, Semimajor axis, Inclination, Ascending Node, Arg. of Periapsis, time at perihelion
	Planet(std::string n, double m, double r, double e, double a, double i, double aN, double periapsis, double T) :
		name(n), mass(m), radius(r), eccentricity(e), semimajor(a), inclination(i), ascendingNode(aN), longitudePerihelion(periapsis), timeAtPerihelion(T) {
		orbitalPeriod = 365.256898326 * pow(a, 1.5) / sqrt(1 + m);
		dailyMotion = PI / orbitalPeriod;
		M0 = 0;
	};
};

struct Moon : Component<Moon> {
	Entity parent;
	Component<Planet> planet;

	Moon(Planet planet) : planet(planet) {}
};
