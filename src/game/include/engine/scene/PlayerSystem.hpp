#pragma once

#include <engine/core/Context.hpp>
#include <engine/core/System.hpp>
#include <engine/events/EventSystem.hpp>
#include <engine/events/KeyboardEvent.hpp>
#include <engine/events/MouseEvent.hpp>
#include <engine/core/SimulateEvent.hpp>
#include <engine/core/WindowSystem.hpp>
#include <engine/core/SettingsSystem.hpp>
#include <engine/graphics/RendererSystem.hpp>
#include <engine/audio/AudioSystem.hpp>
#include <engine/ui/UISystem.hpp>
#include <engine/scene/SceneGraphSystem.hpp>
#include <engine/scene/OrbitalSimulationSystem.hpp>
#include <engine/scene/Entity.hpp>
#include <engine/scene/Transform.hpp>
#include <engine/scene/Drawable.hpp>
#include <engine/scene/Planet.hpp>



class PlayerSystem : public System {
private:
  SettingsSystem* m_settings;
  EventSystem* m_events;
  SceneGraphSystem *m_scene;
  RendererSystem* m_renderer;
  AudioSystem* m_audio;
  UISystem* m_ui;
  OrbitalSimulationSystem* m_orbitals;
	WindowSystem* m_window;

  Entity m_skyboxCamera;
  Entity m_mainCamera;
  Entity m_cockpitCamera;
  Entity m_minimapCamera;

  Entity playerIcon;

  Entity parentEntity;
  Transform::Handle cameraTransform;
  Transform::Handle cockpitCamTransform;
  Transform::Handle skyboxCamTransform;
  Camera::Handle camera;

  bool m_keyState[SDL_NUM_SCANCODES];
  glm::vec2 mouseMove = glm::vec2(0, 0);
  bool movementMode = 1;
  double speed = 10;

  double velocity = 0;
  double acceleration = 2;
  double mass = 1000.0;
  double momentum = 0;

  glm::dmat4 directionRot;
  Transform::Handle followObj;


  void handleKeyboard(KeyboardEvent e);
  void handleMouse(MouseEvent e);

  void update(float dt);

  bool m_hasControl = true;

public:
  CONSTRUCT_SYSTEM(PlayerSystem) {}

  bool startup() override;
  void shutdown() override;

  void setPosition(glm::dvec3 p) {
    cameraTransform->position = p;
  }

  void setCockpitPosition(glm::dvec3 p) {
    cockpitCamTransform->position = p;
  }

  void setHasControl(bool val) {
    m_hasControl = val;
  }

  glm::dmat4 getShipRotation() {
    return directionRot;
  }

  glm::dmat4 getRotation() {
    return cameraTransform->rotation;
  }

  glm::dvec3 getWorldPosition() {
    return cameraTransform->lastGlobalTransform.pos;
  }

  glm::dvec3 getCockpitPosition() {
    return cockpitCamTransform->position;
  }

  double getVelocity() {
    return velocity;
  }


  glm::dvec3 getPosition() {
    return cameraTransform->position;
  }

  const bool* getKeyTable() { return m_keyState; };

  std::string getParentName() {
		if (parentEntity.valid() && parentEntity.has_component<Planet>()){
			return parentEntity.component<Planet>()->name;
		}
		else return "Sun";
  }

  void setRotation(glm::dmat4 r) {
    cameraTransform->rotation = r;
    cockpitCamTransform->rotation = r;
    skyboxCamTransform->rotation = r;
    directionRot = r;
  }

  void attachToParent(Entity e) {
    
    if (parentEntity.valid() && parentEntity.has_component<Planet>()) {
      m_orbitals->minimapItems[parentEntity.id()].component<Drawable>()->material.emissiveColor = glm::vec4(1.0, 0.0, 0, 1.0);
    }
    cameraTransform->parent = e.component<Transform>();
    parentEntity = e;
    m_orbitals->minimapItems[e.id()].component<Drawable>()->material.emissiveColor = glm::vec4(0.0, 1.0, 0, 1.0);
  }
};