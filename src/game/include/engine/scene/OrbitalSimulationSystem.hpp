#pragma once

#include <engine/core/Context.hpp>
#include <engine/core/System.hpp>
#include <engine/events/EventSystem.hpp>
#include <engine/scene/SceneGraphSystem.hpp>
#include <engine/core/SimulateEvent.hpp>
#include <engine/scene/Entity.hpp>
#include <engine/scene/Planet.hpp>
#include <vector>
#include <engine/graphics/RendererSystem.hpp>
#include <engine/core/SettingsSystem.hpp>



class OrbitalSimulationSystem : public System {
private:
  SettingsSystem* m_settings;
	EventSystem* m_events;
	SceneGraphSystem *m_scene;
  RendererSystem *m_renderer;

	Geometry defaultGeom;
  Geometry atmosphereGeom;
  Geometry orbitTorus;
  Geometry minimapItem;
	
  Material moonMat;
	Material trajectoryMat;
  Material atmosphereMat;
	Material ttfMaterial;
  Material waterMat;
  Material minimapItemMaterial;

  std::unordered_map<std::string, Material> planetMaterials;

	std::vector<Entity> trajectories;  

	glm::dvec3 applyKepler(Planet::Handle planet, float dt);
  SharedTexture2D m_terrainNoise;

public:
	CONSTRUCT_SYSTEM(OrbitalSimulationSystem) {}

	const float solarRadius = 696342;
	const float scaleFactor = 1;
	const float distanceScale = 0.015;

  Material playerIconMaterial;
  Geometry playerIconGeometry;
  std::map<Entity::Id, Entity> minimapItems;

  Entity addPlanet(Transform::Handle sun, std::string n, double m, double r, double e, double a, double i, double N, double P, double T, std::string planetType = "earth");

	bool startup() override;
	void shutdown() override;

	void simulateOrbitals(float dt, float totalTime);

	double solarToWorld(double solar);
  double AUToWorld(double au);

	double solarToAU(double s);
	double AUToSolar(double au);

  double worldToSolar(double w);

  double worldToAU(double w);
  glm::dvec3 worldToAU(glm::dvec3 w);

	void showTrajectories();

};
