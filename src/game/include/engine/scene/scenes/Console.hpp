#include <engine/scene/Scene.hpp>
#include <engine/scene/Transform.hpp>
#include <engine/scene/Drawable.hpp>
#include <engine/audio/SoundSource.hpp>

class Console {
private:
  Entity cube;

  enum class State {
    FadingIn,
    FadingTextOut,
    MovingA,
    Playing
  };

  struct Shot : Component<Shot> {
    glm::vec3 vel;
    bool friendly;
    float lifetime;
  };

  struct Enemy : Component<Enemy> {
    glm::vec3 vel;
    float lifetime;
    int lifes;
  };

  State state = State::FadingIn;
  float timer = 0;

  const float FADE_IN_TIME = 3.0f;
  const float FADE_OUT_TIME = 5.5f;
  const float MOVE_A_TIME = 7.5f;

  Entity text;

  Entity background;

  Entity letterW;

  Entity soundSource;

  Entity planet;
  Drawable::Handle planetDrawable;
  Transform::Handle planetTransform;


  Drawable::Handle textDrawable;
  Drawable::Handle letterADrawable;
  Drawable::Handle backgroundDrawable;

  Transform::Handle letterATransform;

  SoundSource::Handle soundSourceAudio;
  Transform::Handle soundSourceTransform;

  glm::dvec3 aStartPos;
  glm::dvec3 aTargetPos;

  glm::quat aStartRot;
  glm::quat aTargetRot;

  glm::dvec3 playerSpeed;

  
  std::map<std::string, Geometry> enemyGeometries;
  int currentEnemy = 0;
  std::string enemyOrder = "edgeofspace";

  Geometry shotGeom;
  Material shotMaterial;

  Material enemyMaterial;

  uint32_t consolePassId;

  void spawnShot(glm::dvec3 pos, glm::dvec3 vel, bool friendly);
  void updateShots(double dt);

  void spawnEnemy(glm::dvec3 pos, glm::dvec3 vel);
  void updateEnemies(double dt);

  void updateFFT();

  double planetRotation;
  void updatePlanet(double dt);

  float timeSinceLastShot = 0;

  float nextEnemySpawnTime = 2;

  std::vector<Entity> shots;
  std::vector<Entity> enemies;

public:
  EventSystem* m_events;
  RendererSystem* m_renderer;
  SceneGraphSystem* m_sceneGraph;
  AudioSystem* m_audio;

  void startup();
  bool update(const SimulateEvent& e, const bool* keyTable);
  void pause();
  void play();
};