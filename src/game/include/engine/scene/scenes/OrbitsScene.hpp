#pragma once
#include <engine/scene/Scene.hpp>
#include <engine/scene/OrbitalSimulationSystem.hpp>
#include <engine/scene/PlayerSystem.hpp>
#include <engine/audio/SoundSource.hpp>

class OrbitsScene : public Scene {
private:
  OrbitalSimulationSystem* m_orbitals;
  PlayerSystem* m_player;

  SoundSource::Handle soundSource;

  Entity sun;

  Entity mercury;
  Entity venus;
  Entity earth;
  Entity mars;
  Entity jupiter;
  Entity saturn;
  Entity uranus;
  Entity neptune;


public:
  CONSTRUCT_SCENE(OrbitsScene) { };

  bool startup() override;
  void shutdown() override;

};