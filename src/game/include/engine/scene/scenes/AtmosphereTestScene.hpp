#pragma once
#include <engine/scene/Scene.hpp>
#include <engine/scene/OrbitalSimulationSystem.hpp>
#include <engine/scene/PlayerSystem.hpp>
#include <engine/audio/SoundSource.hpp>
#include <engine/scene/scenes/Console.hpp>

class AtmosphereTestScene : public Scene {
private:
  OrbitalSimulationSystem* m_orbitals;
  PlayerSystem* m_player;

  Console m_console;

  Light::Handle sunLight, sunLight2;
  Entity sun;
  Entity sun2;
  Entity skybox;

  Entity loadingText;

  Entity mercury;
  Entity venus;
  Entity earth;
  Entity mars;
  Entity jupiter;
  Entity saturn;
  Entity uranus;
  Entity neptune;

  Entity cockpit;
  Entity cockpitSun;
  Light::Handle cockpitSunLight;

  Entity consoleCamera;
  SoundSource::Handle consoleSoundSource;
  SoundSource::Handle cockpitSoundSource;


  Entity planetViewCam;

  Material cockpitMaterial, sunMaterial, skyboxMaterial, windshieldMaterial;
  Geometry cockpitGeometry, sunGeometry, skyboxGeometry, planetViewGeom, logoViewGeom;

  Geometry consoleGeom, consoleFrontLeftGeom, consoleFrontRightGeom, consoleBackLeftGeom, consoleBackRightGeom, consoleMiddleLeftGeom, consoleMiddleRightGeom, windShieldGeom;
  SharedTexture2D outOfOrderTexture;

  SharedShaderProgram defaultPbrProg;

  double planetRotationSpeed = 0;
  Transform::Handle planetViewTransform;

  Transform::Handle logoViewTransform;


  std::shared_ptr<Sound> soundTrackIntro, soundTrackMain;

  bool resourceLoadingFinished = false;
  bool mainSceneRunning = false;
 
  bool isInterpolatingCamera = false;
  float interpolationTime, totalInterpolationTime;
  glm::dvec3 camStartPos;
  glm::dvec3 camTargetPos;

  glm::quat camStartRot;
  glm::quat camTargetRot;

  const glm::dvec3 CAM_CONSOLE_POS = { 0, -0.24977, -2.79714 };
  const glm::mat4 CAM_CONSOLE_ROT = glm::rotate(-1.05, glm::dvec3{ 1.0, 0.0, 0.0 });

  bool isFocusedOnConsole = true;

  bool didOneFrame = false;

public:
  CONSTRUCT_SCENE(AtmosphereTestScene) { };
  
  void switchToMainScene();
  void loadMainSceneResources();
  void switchConsole();

  bool startup() override;
  void shutdown() override;

};