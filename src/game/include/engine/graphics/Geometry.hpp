#pragma once
#include <ACGL/ACGL.hh>
#include <ACGL/OpenGL/Objects.hh>

using namespace ACGL::OpenGL;

struct Geometry {
  SharedVertexArrayObject vao;
};